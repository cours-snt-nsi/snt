---
title: Accueil
hide:
    -toc
    -navigation
---
!!! success "Site SNT Lycee Jean Prevost"

    ![](./assets/LogoLaForge.svg){ width=50% }![](./assets/qrcode_atelier_forge.png){ width=20% align=right}

    Ce site est le document d'accompagnement de la SNT ET NSI (2024-2025).

!!! abstract "Bienvenue sur notre plateforme de cours en ligne !"

    ## Sciences Numériques et Technologie 
    Explorez les concepts fondamentaux du numérique et de la technologie à travers des cours vidéo, des exercices pratiques et des projets stimulants. Développez vos compétences en programmation, en analyse de données et en cybersécurité.  

    ## Consignes de rentrée

    La cours s'articule entre travaux pratiques en demi-groupes à raison d'une heure tous les quinze jours et cours-activités en classe entière à raison d'une heure par semaine.

    Evaluation:

    - Evaluation écrite : sous forme de questions classiques et QCM. Les élèves ont droit à tous leurs documents de cours. Chaque évaluation comporte des questions de l'évaluation précédente ou se rapportant à du travail qui était à faire à la maison.
    - Evaluation orale : dans l'année les élèves auront à intervenir devant la classe sur des sujets divers.
    - Evaluation pratique : certaines activités pratiques seront évaluées, sous forme de bonus à la moyenne.
    - Evaluation de l'investissement et du sérieux : lors de toutes les activités, sous forme de bonus à la moyenne.



