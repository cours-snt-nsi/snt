---
author: Steeve PYTEL
title: 🏢 Exercices
---

# Exercices

!!! abstract "Activitées Pixel'Art"

    1. [Easy](https://capytale2.ac-paris.fr/web/c/dbee-644319){target='_blank'}
    2. [Drapeau](https://capytale2.ac-paris.fr/web/c/125f-1447786){target='_blank'}
    3. [Zig](https://capytale2.ac-paris.fr/web/c/4cc1-567778){target='_blank'}
    4. [Echec](https://capytale2.ac-paris.fr/web/c/ab28-802220){target='_blank'}

!!! abstract "Capytale"

    1. [Coloriage](https://capytale2.ac-paris.fr/web/c/073e-1952982){target='_blank'}
    2. [Manipulation](https://capytale2.ac-paris.fr/web/c/6271-3574308){target='_blank'}
    3. [Cache-cache](https://capytale2.ac-paris.fr/web/c/9cdd-3700092){target='_blank'}
    4. [Cache-Console](https://capytale2.ac-paris.fr/web/c/6218-1490234){target='_blank'}
