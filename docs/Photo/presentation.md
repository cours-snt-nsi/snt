---
author: à compléter
title: 📋 La photographie numérique
---


!!! abstract "Présentation Résumé"

    Les technologies de la photographie argentique ont eu une évolution très lente, liée aux progrès en optique, mécanique et chimie. Ce n’est plus du tout le cas de l’évolution actuelle, davantage due aux algorithmes qu’à la physique : algorithmes de développement et d’amélioration de l’image brute, algorithmes d’aide à la prise de vue. Cet exemple est caractéristique des façons de procéder de la révolution informatique par rapport aux approches traditionnelles.
    La photographie numérique présente un coût marginal très faible et une diffusion par internet facile et immédiate : chaque jour, des milliards de photos sont prises et partagées.

!!! note "01. Video Introduction Photographie Numérique"

	<div class="centre">
	<iframe width="1280" height="720" src="https://ladigitale.dev/digiview/#/v/66b0d0ecf2fec" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
	</div>

|Contenus|Capacités attendues|
|:--- | :--- |
|Photosites, pixels, résolution (du capteur, de l'image), profondeur de couleur|Distinguer les photosites du capteur et les pixels de l'image en comparant les résolutions du capteur et de l'image selon les réglages de l'appareil.|
|Métadonnées EXIF|Retrouver les métadonnées d'une photographie.|
|Traitement d'image|Traiter par programme une image pour la transformer en agissant sur les trois composantes de ses pixels.|
|Rôle des algorithmes dans les appareils photo numériques|Expliciter des algorithmes associés à la prise de vue. Identifier les étapes de la construction de l'image finale.|
