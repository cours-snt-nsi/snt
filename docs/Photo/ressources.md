---
author: à compléter
title: 📚 Ressources
---

# La Photographie

## Introduction

!!! abstract "Présentation"

    Dans cette activité, les élèves visionnent une vidéo d’introduction à la photographie numérique et rédigent un texte pour résumer ce qu’ils ont compris.



??? note "01. Introduction"

	<div class="centre">
	<iframe 
	src="../a_telecharger/01/01. Introduction - PHOTOS NUMERIQUES.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>


[01. Introduction](https://forge.apps.education.fr/ressources/snt/-/tree/main/docs/Photo/a_telecharger/01){ .md-button target="_blank" rel="noopener" }



## Histoire

!!! abstract "Présentation"

    Dans cette activité, l’enseignant.e présente quelques dates clefs sur la photo numérique en relation des événements historiques concomitants.


??? note "02. Histoire"

	<div class="centre">
	<iframe 
	src="../a_telecharger/02/02. Histoire - PHOTOS NUMERIQUES.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>

[02. Histoire](https://forge.apps.education.fr/ressources/snt/-/tree/main/docs/Photo/a_telecharger/02){ .md-button target="_blank" rel="noopener" }

## EXIF Présentation

!!! abstract "Présentation"

    Dans cette activité, on définit ce que sont les données EXIF, comment les lire et à quoi elles servent. On termine par une réflexion sur les aspects positifs et négatifs de ces données.


??? note "03. introduction EXIF"

	<div class="centre">
	<iframe 
	src="../a_telecharger/03/03. introduction EXIF - PHOTOS NUMERIQUES.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>

[03. introduction EXIF](https://forge.apps.education.fr/ressources/snt/-/tree/main/docs/Photo/a_telecharger/03){ .md-button target="_blank" rel="noopener" }


## Métadonnées EXIF 

!!! abstract "Présentation"

    Dans cette activité, l’enseignant.e définit ce que sont les données EXIF, comment les lire et à quoi elles servent. La réflexion se termine sur les aspects positifs et négatifs de ces données.


??? note "04. EXIF"

	<div class="centre">
	<iframe 
	src="../a_telecharger/04/04. EXIF - PHOTOS NUMERIQUES.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>

[04. EXIF](https://forge.apps.education.fr/ressources/snt/-/tree/main/docs/Photo/a_telecharger/04){ .md-button target="_blank" rel="noopener" }

## Formats et couleurs

!!! abstract "Présentation"

    Dans cette activité, l’enseignant.e fait découvrir les notions de format et de couleurs à travers des activités de manipulation en ligne.


??? note "05. Formats et couleurs"

	<div class="centre">
	<iframe 
	src="../a_telecharger/05/05. Formats et couleurs - PHOTOS NUMERIQUES.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>

[05. Formats et couleurs](https://forge.apps.education.fr/ressources/snt/-/tree/main/docs/Photo/a_telecharger/05){ .md-button target="_blank" rel="noopener" }

## Principe des images numériques

!!! abstract "Présentation"

    Dans cette activité, les élèves découvrent les grands principes de l’image numérique : pixels, synthèse additive des couleurs, résolution, définition, etc.


??? note "06. Principe des images numériques"

	<div class="centre">
	<iframe 
	src="../a_telecharger/06/06. Principe des images numériques - PHOTOS NUMERIQUES.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>

[06. Principe des images numériques](https://forge.apps.education.fr/ressources/snt/-/tree/main/docs/Photo/a_telecharger/06){ .md-button target="_blank" rel="noopener" }

## Pixels définition et résolution

!!! abstract "Présentation"

    Dans cette activité, l’enseignant.e fait découvrir les notions de pixels, définition et résolution à travers des activités de manipulation en ligne.


??? note "07. Pixels définition et résolution"

	<div class="centre">
	<iframe 
	src="../a_telecharger/07/07. Pixels définition et résolution - PHOTOS NUMERIQUES.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>

[07. Pixels définition et résolution](https://forge.apps.education.fr/ressources/snt/-/tree/main/docs/Photo/a_telecharger/07){ .md-button target="_blank" rel="noopener" }

## Capteurs photos

!!! abstract "Présentation"

    Dans cette activité, les élèves découvrent les grands principes du fonctionnement des capteurs photos (filtres, photosites, etc.)


??? note "08. Principe des capteurs photos"

	<div class="centre">
	<iframe 
	src="../a_telecharger/08/08. Principe des capteurs photos - PHOTOS NUMERIQUES.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>

[08. Principe des capteurs photos](https://forge.apps.education.fr/ressources/snt/-/tree/main/docs/Photo/a_telecharger/08){ .md-button target="_blank" rel="noopener" }

## Traitement d'images

!!! abstract "Présentation"

    Dans cette activité, l’enseignant.e présente et on implémente en Python à l’aide de la bibliothèque PIL quelques algorithmes de traitement d’images.


??? note "09. Initiation au traitement d'images"

	<div class="centre">
	<iframe 
	src="../a_telecharger/09/09. Initiation au traitement d_images - PHOTOS NUMERIQUES.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>

[09. Initiation au traitement d'images](https://forge.apps.education.fr/ressources/snt/-/tree/main/docs/Photo/a_telecharger/09){ .md-button target="_blank" rel="noopener" }

##  Couleurs pixels 

!!! abstract "Présentation"

    Dans cette activité au format Notebook, l’enseignant.e présente la synthèse additive des couleurs et introduit la notion de pixels et de codage RVB en utilisant le module ipythonblock.


??? note "10. Couleurs pixels image - Manipulation"

	<div class="centre">
	<iframe 
	src="../a_telecharger/10/10. Couleurs, pixels, image - Manipulation - PHOTOS NUMERIQUES.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>

??? note "10. Jupyter Notebook"

	<div class="centre">
	<iframe 
	src="../a_telecharger/10/Jupyter Notebook.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>

[10. Couleurs pixels image - Manipulation et Notebook](https://forge.apps.education.fr/ressources/snt/-/tree/main/docs/Photo/a_telecharger/10){ .md-button target="_blank" rel="noopener" }