title: aide préparation cours

# Sites divers pour illustrer le cours


- [CodEx](https://codex.forge.apps.education.fr/){target="_blank"} exercices d'apprentissage de l'algorithmique et de la programmation par des professeurs d'informatique du secondaire et du supérieur

- [e-nsi ressources](https://e-nsi.forge.aeif.fr/){target="_blank"}  des professeurs d'informatique du secondaire et du supérieur, qui produisent collectivement des ressources pédagogiques sur le thème des sciences informatiques

- - - - 

- [Compilation des programmes de SNT et NSI](https://codeberg.org/jrm-omg/BO_programmes_de_SNT_et_NSI){target="_blank"} 

- [Programmes de la spécialité NSI](https://eduscol.education.fr/2068/programmes-et-ressources-en-numerique-et-sciences-informatiques-voie-g){target="_blank"} 

- [Eduscol NSI](https://nsi-snt.ac-normandie.fr/ressources-d-accompagnement-eduscol-pour-nsi){target="_blank"} Ressources d'accompagnement Eduscol pour la NSI.



- [Lumni 1NSI](https://www.lumni.fr/lycee/premiere/nsi){target="_blank"}  et [Lumni TNSI](https://www.lumni.fr/lycee/terminale/nsi){target="_blank"} Contenus pédagogiques pour préparer les cours avec Lumni.


- [Terra numerica](http://terra-numerica.org/){target="_blank"} projet pour la diffusion de la culture des sciences du numérique émanant du CNRS, de l'Inria et de l'Université Côte d’Azur

- [Les décodeuses du numérique](https://www.ins2i.cnrs.fr/fr/les-decodeuses-du-numerique){target="_blank"} mettre en avant la diversité des recherches en sciences du numérique et contribuer à briser les stéréotypes qui dissuadent les femmes de s’engager dans cette voie


- [Inria](https://learninglab.inria.fr/){target="_blank"}  conçoit des MOOC en sciences du numérique et accompagne la recherche en e-éducation 

- - - - 

- [SIte](https://www.nsi-premiere.fr/){target="_blank"} le livre « Numérique et Sciences Informatiques, 30 leçons avec exercices corrigés »

- [Documentation officielle Python](https://docs.python.org/fr/3/){target="_blank"} 

- [Tutoriel officiel](https://docs.python.org/fr/3/tutorial/index.html){target="_blank"}

- [La référence du langage Python](https://docs.python.org/fr/3/reference/index.html){target="_blank"}

- - - - 



- [AlgoGraphe](https://frederic-zinelli.gitlab.io/algographe/){target="_blank"} par Frederic Zinelli 
(une application dédiée à la création de graphes, leur affichage, leur conversion en structure de données, et l’animation d’algorithmes sur les-dits graphes...) ; [Documentation complète](https://frederic-zinelli.gitlab.io/algographe/docs/){target="_blank"} ; [Dépôt](https://gitlab.com/frederic-zinelli/algographe){target="_blank"} et [dossier téléchargeable]( https://gitlab.com/frederic-zinelli/algographe/-/tree/main/dist?ref_type=heads){target="_blank"}


- [Création de graphes, arbres...](http://fred.boissac.free.fr/AnimsJS/DariushGraphes/index.html){target="_blank"}  par Fred Boissac

- [Vittascience](https://fr.vittascience.com/){target="_blank"}  plateforme éducative pensée pour l'apprentissage du codage, qui propose des outils pour l'enseignement (programmer des cartes, des  robots, des jeux)

- [Recursion visualizer](https://www.recursionvisualizer.com/?function_definition=def%20virfib(n)%3A%20%20%20if%20n%20%3D%3D%200%3A%20%20%20%20%20return%200%20%20%20if%20n%20%3D%3D%201%3A%20%20%20%20%20return%201%20%20%20else%3A%20%20%20%20%20return%20virfib(n%20-%201)%20%2B%20virfib(n%20-%202)%20%20%20%20%20&function_call=virfib(4)){target="_blank"} visualiser la récursivité

- [Animation des algorithmes](http://fred.boissac.free.fr/AnimsJS/Dariush_Anims/index.html){target="_blank"}  par Fred Boissac

- [Recherche textuelle : Boyer-Moore-Horspool](https://www.youtube.com/watch?v=4ocIC-mWfqg){target="_blank"}  vidéo de Fred Boissac

- [UP Simulator](http://goupill.fr/up/){target="_blank"} compilateur pour un modèle de microprocesseur (pseudo Python -> assembleur -> Binaire) et son  [Simulateur](http://goupill.fr/up/up.html){target="_blank"} 

- [CodePuzzle](https://www.codepuzzle.io/){target="_blank"} Générateur de “puzzles de Parsons”, apprendre en corrigeant du code par Laurent Abbal ;  [Documentation](https://codepuzzle-io.github.io/documentation/04-entrainements-devoirs/){target="_blank"}

- [Fondamentaux de la sécurité réseau](https://eduscol.education.fr/sti/si-ens-paris-saclay/ressources_pedagogiques/informatique-debranchee-dechiffrez-cest-gagne#description){target="_blank"}  Informatique débranchée : Jeu Déchiffrez c’est gagné

- [Genumsi](https://genumsi.inria.fr/){target="_blank"}
(Où l'on peut créer un QCM pour les cours de NSI)

- - - - - 




- [evalNSI](https://jcottin.forge.aeif.fr/e-nsi/logiciel_evalNSI/logiciel_evalNSI/){target="_blank"} Logiciel pour générer des sujets au format Latex à partir des sujets de BAC, par Joffrey Cottin.



- [Dépôt](https://drive.google.com/drive/folders/1zcExCCLpKIaIJzpdgXw7oN2ucA1-Vci2){target="_blank"} Projet Timeline Informatique par Paul Faure-Jeunot.


- [CodePen par Dimitar](https://codepen.io/justd/pen/yydezN){target="_blank"} Démonstration des propriétés de Flexbox (CSS)


- - - - - 

- [Unblocked Unchained](https://pablockchain.fr/){target="_blank"} une introduction  aux blockchains et une  [Vidéo](https://www.youtube.com/watch?v=bBC-nXj3Ng4){target="_blank"} sur le fonctionnement du Bitcoin


- [Articles](https://professeurb.github.io/articles/){target="_blank"}
(Où l'on peut trouver un certain nombre d'articles traîtant d'aspects variés de l'informatique…)

- [RosettaCode](https://rosettacode.org/wiki/Rosetta_Code){target="_blank"} present solutions to the same task in as many different languages as possible, to demonstrate how languages are similar and different...

- [Puzzle Light UP](https://fr.puzzle-light-up.com//){target="_blank"} casses têtes en ligne