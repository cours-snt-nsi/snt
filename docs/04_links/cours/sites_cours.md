title: Sites de collègues

# Quelques sites de professeurs de NSI

???info "Site de Andjekel : cours de SNT, 1NSI et TNSI"
    [Site](https://www.levavasseur.xyz/){target="_blank"}

???info "Site de Gisele Bareux : cours 1NSI, TNSI et BTS Systèmes Photoniques SP"
    [Site](http://gisele.bareux.free.fr/){target="_blank"}


???info "Site de Thomas Beline"
    [NSI](https://kxs.fr/cours/){target="_blank"}


???info "Site de Mathilde Boehm, Charles Poulmaire et Pascal Remy"
    [Première NSI](https://cours-nsi.forge.apps.education.fr/premiere/introduction.html){target="_blank"}

???info "Chaine Youtube de Frederic Boissac"
    [Chaine](https://www.youtube.com/channel/UCY84E27tcYNBQU2x-OHcROA){target="_blank"}
    

???info "Le site de Vincent Bouillot"
    === "Lien 1NSI"
        [Première NSI](https://ferney-nsi.gitlab.io/premiere/){target="_blank"}
    === "Lien TNSI"
        [Terminale NSI](https://ferney-nsi.gitlab.io/terminale/){target="_blank"}

???info "Site de Alain Bussier"
    [NSI et MP2I](https://alainbusser.frama.io/NSI-IREMI-974/){target="_blank"}

???info "Les sites de Franck Chambon"
    [site](https://fchambon.forge.apps.education.fr/classe/8-productions/){target="_blank"}
   
???info "Les sites de Mireille Coilhac"
    === "Première NSI"
        [Première NSI](https://mcoilhac.forge.apps.education.fr/site-nsi//){target="_blank"}
    === "Terminale NSI"
        [Terminale NSI](https://mcoilhac.forge.apps.education.fr/term//){target="_blank"}

???info "Les sites de Guillaume Conan"
    === "Cours d’informatique de BCPST"
        [BCPST-1](https://edenmaths.gitlab.io/bcpst1/2022_23/INFORMATIQUE/){target="_blank"} et [BCPST-2](https://edenmaths.gitlab.io/bcpst1/2022_23/INFORMATIQUE/B2/){target="_blank"}
    === "Dépôts Git de NSI"
        [1NSI](https://gitlab.com/lyceeND/1ere/-/tree/master/2019_20){target="_blank"} et [TNSI](https://gitlab.com/lyceeND/tale/-/tree/master/2020_21){target="_blank"}
    === "Cours d'IUT"
        [IUT INFO 1](https://informathix.tuxfamily.org/?q=node/5){target="_blank"} et [IUT INFO 2](https://informathix.tuxfamily.org/?q=node/16){target="_blank"}
    === "Cours d'info de MP et MP*"
        [cours](https://informathix.tuxfamily.org/?q=node/14){target="_blank"}
    === "Cours du DU de NSI"
        [cours](https://gitlab.com/GiYoM/du/-/tree/master/){target="_blank"}
    === "Dépôt GIT du cours de Python  en Licence"
        [L1](https://github.com/Informathix/Complements_Info_L1_UCO_Angers){target="_blank"} ; [L2](https://github.com/Informathix/UCO_L2){target="_blank"} ; [L3](https://github.com/Informathix/UCO_L3){target="_blank"}

???info "Le site de Laurent Cooper"
    === "Introduction"
        [Site](http://lycee.educinfo.org/index.php?page=introduction&activite=signedint){target="_blank"}
    === "SNT"
        [Site](http://lycee.educinfo.org/index.php?page=SNT){target="_blank"}
    === "Première NSI"
        [Première NSI](http://lycee.educinfo.org/index.php?page=NSI1){target="_blank"}
    === "Terminale NSI"
        [Terminale NSI](http://lycee.educinfo.org/index.php?page=NSIT){target="_blank"}

???info "Le site de Luc Courbon"
    === "Lien 1NSI"
        [Première NSI](https://lycee-maths.info/NSI%201%C3%A8re/Pr%C3%A9sentation/){target="_blank"}
    === "Lien TNSI"
        [Terminale NSI](https://lycee-maths.info/NSI%20Terminale/Cours/){target="_blank"}


???info "Les cours de Dominique Filoé : niveau première"
    === "Les réseaux"
        [Lien](http://siingenieur.free.fr/nsi/site_reseau/index.html){target="_blank"}
    === "HTML/CSS"
        [Lien](http://siingenieur.free.fr/nsi/site_htmlcss/index.html){target="_blank"}
    === "Protocole http et formulaire"
        [lien](http://siingenieur.free.fr/nsi/site_httpgetpost/){target="_blank"}
    === "Javascript"
        [lien](http://siingenieur.free.fr/nsi/site_javascript/){target="_blank"}
    === "Côté serveur"
        [Lien](http://siingenieur.free.fr/nsi/site_serveur/){target="_blank"}
    === "Entrées/sorties"
        [lien](http://siingenieur.free.fr/nsi/site_ihm/){target="_blank"}
    === "Traitement tables"
        [lien](http://siingenieur.free.fr/nsi/site_courstable/){target="_blank"}

???info "Les cours  de Dominique Filoé : niveau Terminale"
    === "Données en table"
        [Lien](http://siingenieur.free.fr/nsi/site_table/){target="_blank"}
    === "Modèle relationnel"
        [Lien](http://siingenieur.free.fr/nsi/site_modelerelationnel/){target="_blank"}
    === "SGBD"
        [lien](http://siingenieur.free.fr/nsi/site_sql/){target="_blank"}
    === "Réseau et Internet"
        [Lien](http://siingenieur.free.fr/nsi/site_reseau/){target="_blank"}
    === "Routage"
        [lien](http://siingenieur.free.fr/nsi/site_routage/){target="_blank"}



???info "Le site de Sébastien Hoarau"
    [Python des Neiges](https://sebhoa.gitlab.io/iremi/){target="_blank"}


???info "Le site de Romain Janvier"
    === "Lien 1NSI"
        [Première NSI](http://nsi.janviercommelemois.fr/){target="_blank"}
    === "Lien Exercices 1NSI"
        [Première NSI](https://romainjanvier.forge.aeif.fr/nsipremiere/){target="_blank"}
    === "Lien TNSI"
        [Terminale NSI](http://nsiterminale.janviercommelemois.fr/){target="_blank"}               



???info "Site de Frédéric Junier"
    [1NSI](https://frederic-junier.gitlab.io/parc-nsi/chapitre21/http-git/){target="_blank"}

???info "Le dépôt de David Landry"
    === "Lien 1NSI"
        [Première NSI](https://forge.apps.education.fr/nsi-clemenceau/nsi-1ere){target="_blank"}

???info "Le site de Gilles Lassus"
    === "Lien 1NSI"
        [Première NSI](https://glassus.github.io/premiere_nsi/){target="_blank"}
    === "Lien TNSI"
        [Terminale NSI](https://glassus.github.io/terminale_nsi/){target="_blank"}




???info "Le site de Frédéric Mandon"
    === "Lien 1NSI"
        [Première NSI](http://www.maths-info-lycee.fr/nsi_1ere.html){target="_blank"}
    === "Lien TNSI"
        [Terminale NSI](http://www.maths-info-lycee.fr/nsi.html){target="_blank"} 

???info "Les vidéos de Pierre Marquestaut"
    === "Cours en vidéo"
        [site](https://peertube.lyceeconnecte.fr/c/pierre.marquestaut_channel/videos?s=1){target="_blank"}

???info "Le site de Yves Moncheaux"
    === "Cours première NSI"
        [site](https://clogique.fr/nsi/premiere/){target="_blank"}


        

???info "Le site de Fabrice Nativel"
    === "Cours de première et de terminale NSI"
        [NSI](https://fabricenativel.github.io/){target="_blank"}



???info "Le site de Denis Quenton"
    === "Lien 1NSI"
        [Première NSI]( https://dquenton.forge.apps.education.fr/nsi-premiere-specialite/){target="_blank"}
    === "Lien TNSI"
        [Terminale NSI](https://dquenton.forge.apps.education.fr/nsi-terminale-specialite/){target="_blank"}

???info "Dépôt de Nicolas Reveret"
    [Dépôt Forge](https://forge.apps.education.fr/nreveret){target="_blank"}

???info "Le site des pixees de David Roche"
    === "Informatique au lycée"
        [Site](https://pixees.fr/informatiquelycee/){target="_blank"}
    === "Lien ancien site 1NSI"
        [Première NSI](https://pixees.fr/informatiquelycee/n_site/nsi_prem.html){target="_blank"}
    === "Lien ancien site TNSI"
        [Terminale NSI](https://pixees.fr/informatiquelycee/n_site/nsi_term.html){target="_blank"}



???info "Le site de Pierre-Alain Sallard"
    === "Cours de terminale NSI"
        [NSI](https://pasallard.gitlab.io/terminale_nsi_voltaire/){target="_blank"}


???info "Le site Rodrigo Schwencke"
    === "Lien 1NSI"
        [Première NSI](https://eskool.gitlab.io/1nsi/){target="_blank"}
    === "Lien TNSI"
        [Terminale NSI](https://eskool.gitlab.io/tnsi/){target="_blank"}

???info "Site de Eric Tixidor"
    [Allophysique](https://allophysique.com/){target="_blank"}

???info "Le site de Marc Turro"
    [Site](https://marcturro.github.io/prem/premnsi.html){target="_blank"}

???info "Le site de  Cédric Van Rompay"
    === "Projet Moteur de Recherche"
        [Mogahan](https://mogahan.fr/){target="_blank"}  Apprenez à coder en construisant votre propre moteur de recherche



???info "Le site de Grégory Viateau"
    [site de TNSI](http://tnsi.free.fr/){target="_blank"}

???info "Carnets Info de Nathalie Weibel"
    [Site](https://www.carnets.info/){target="_blank"}



???info "Projet eSkool"
    [Repository](https://gitlab.com/eskool/tnsi/-/tree/main){target="_blank"}

    


???info "Site du lycée Langevin"
    [STI2D SIN](http://tsin.langevin-la-seyne.fr/SIN/){target="_blank"}


???info "Le site Mon lycée numérique"
    [site NSI](http://www.monlyceenumerique.fr/index_nsi.html){target="_blank"}

???info "Le site Proalgo"
    [site NSI](https://progalgo.fr/){target="_blank"}


???info "Le site du lycée Toulouse centre"
    [site NSI](https://sites.google.com/view/nsi-toulouse-centre/accueil){target="_blank"}


???info "Le site Maths-code"
    === "Lien 1NSI"
        [Première NSI](http://maths-code.fr/cours/premiere-nsi-2/){target="_blank"}
    === "Lien TNSI"
        [Terminale NSI](http://maths-code.fr/cours/terminale-nsi/){target="_blank"}


???info "icnisnlycee"
    [site](http://icnisnlycee.free.fr/){target="_blank"}
    

???info "Le site Modulo"
    [site](https://dev-apprendre.modulo-info.ch/index.html){target="_blank"}


???info "Cours de Python pour la biologie"
    [site](https://python.sdv.univ-paris-diderot.fr/){target="_blank"}  Introduction à la programmation Python pour la biologie de Patrick Fuchs et Pierre Poulain


???info "InfoForAll"
    [site](https://www.infoforall.fr){target="_blank"}


    
