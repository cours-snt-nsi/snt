title: cours_postBac

# Des liens vers des cours de l'enseignement supérieur

## cours CPGE

- [Site](https://fabricenativel.github.io/cpge-info/){target="_blank"} cours de CPGE de Fabrice Nativel

- [Repository](https://github.com/FayardProf/Magnolia){target="_blank"} Cours de MPSI/MP2I de François Fayard

- [site](http://infoprepa.free.fr/){target="_blank"} classe préparatoire PC du lycée René Cassin de Bayonne Cours par Grégory Viateau




- [cours MP2I](https://github.com/mp2i-info/mp2i-info.github.io){target="_blank"}  de Quentin Fortier


- [CPGE Paradise](https://cpge-paradise.com/){target="_blank"} et [sujets de concours](https://cpge-paradise.com/Sujets2024.php){target="_blank"}


- [École polytechnique](https://www.polytechnique.edu/admission-cycle-ingenieur/documentation/sujets-rapports-statistiques){target="_blank"}

## MOOC SNT/NSI

- [S'initier à la SNT](https://www.fun-mooc.fr/en/courses/sinitier-a-lenseignement-en-sciences-numeriques-et-technologie/){target="_blank"}

- [NSI les fondamentaux](https://www.fun-mooc.fr/en/cours/numerique-et-sciences-informatiques-les-fondamentaux/){target="_blank"}

- [Apprendre à enseigner la NSI](https://www.fun-mooc.fr/en/cours/apprendre-a-enseigner-le-numerique-et-les-sciences-informatiques/){target="_blank"}

- [ Ensemble des contenus de l'initiative MOOC NSI+SNT / CAPES](https://gitlab.com/mooc-nsi-snt){target="_blank"}

## Ressources DIU EIL:  Enseigner l'Informatique au Lycée

- [DIU du Havre](https://mermet.users.greyc.fr/Enseignement/EnseignementInformatiqueLycee/Havre/){target="_blank"} et une [video Bloc 2](https://www.youtube.com/watch?v=Z-Wrpy1yL0A){target="_blank"}

- [Cours 1](https://mermet.users.greyc.fr/Formations/FormationTestsMai2021/){target="_blank"} Cours sur les tests de Bruno Mermet, [Cours 2](https://mermet.users.greyc.fr/Enseignement/EnseignementInformatiqueLycee/Havre/Algorithmique/tests.html){target="_blank"}, [Cours sur le Min/max](https://mermet.users.greyc.fr/Formations/FormationMinMaxAvril2021/){target="_blank"} et [video min/max](https://www.youtube.com/watch?v=3LdhPtv87k0){target="_blank"}


- [Aix-Marseille Université](https://pageperso.lis-lab.fr/~benjamin.monmege/diu-eil-amu){target="_blank"}

- [Université Paris-Saclay](https://www.lri.fr/~kn/diu-nsi_en.html){target="_blank"}

- [Université Grenoble-Alpes](https://diu-eil.gricad-pages.univ-grenoble-alpes.fr/){target="_blank"}

- [Université de Bordeaux Bloc1](https://diu-uf-bordeaux.github.io/bloc1/){target="_blank"}, [Bloc 4](https://diu-uf-bordeaux.github.io/bloc4/){target="_blank"} et [Repository](https://github.com/diu-uf-bordeaux){target="_blank"}

- [Université de Lyon](https://diu-eil.univ-lyon1.fr//){target="_blank"}

- [Université de Lille](https://gitlab-fil.univ-lille.fr/diu-eil-lil/portail/tree/master){target="_blank"}

- [Université de Angers 2](https://gitlab.com/GiYoM/du){target="_blank"}


- [Cours Polytechnique](https://www.enseignement.polytechnique.fr/informatique/INF412/i.php?n=Main.Poly){target="_blank"} Fondements de l'Informatique: Logique, modèles, calculs



