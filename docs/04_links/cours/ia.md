title: IA

## formation sur l'IA


- [Dépot](https://github.com/Math13Net/IA_with_python){target="_blank"}  de Christie Vassilian pour apprendre sur  l'intelligence artificielle


- [Ressource Eduscol](https://eduscol.education.fr/sti/si-ens-paris-saclay/ressources_pedagogiques/dossier-intelligence-artificielle){target="_blank"} Dossier intelligence artificielle



- [Manuel](https://www.ai4t.eu/textbook/){target="_blank"} IA pour les enseignants : un manuel ouvert ; il  traite de la façon dont une branche spécifique de la technologie nommée Intelligence Artificielle (IA) peut changer la façon dont on enseigne.


- [IA génératives](https://digipad.app/p/465149/3979dc305e1d7){target="_blank"} Liens vers une formation sur l'intelligence artificielle générative pour les enseignants.

- [Expérience Hexapawn](https://www.palais-decouverte.fr/fr/explorer-nos-contenus/experiences-dinformatique/une-intelligence-artificielle-en-papier#topMenu){target="_blank"} Construisez une Intelligence Artificielle en papier capable de
jouer à un jeu de plateau

## IA applications 


- [ChatGPT](https://chat.openai.com/auth/login){target="_blank"}  prototype d'agent conversationnel utilisant l'intelligence artificielle, développé par OpenAI et spécialisé dans le dialogue

- [DuckDuckGo AI Chat](https://duckduckgo.com/?q=DuckDuckGo&ia=chat&atb=v349-1){target="_blank"}  Accès anonyme à des modèles d'IA populaires

 

- [DALL·E 2](https://openai.com/index/dall-e-2/){target="_blank"}  can create realistic images and art from a description in natural language

- [Replit AI](https://openai.com/index/dall-e-2/){target="_blank"}  un générateur de code par intelligence artificielle. 

- [Github Copilot](https://github.com/features/copilot){target="_blank"} aider les développeurs en complétant automatiquement le code

- [Checkmath](https://checkmath.com/){target="_blank"} aider à vérifier les devoirs de mathématiques, vérifier les réponse

- [Photomath](https://photomath.com/){target="_blank"} résoudre les problèmes de maths et donner les détails de la résolution étape par étape