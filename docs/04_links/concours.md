title: concours

## Concours Informatique pour les élèves

- [Castor](https://www.castor-informatique.fr/){target="_blank"} Le concours couvre divers aspects de l'informatique : information et représentation, pensée algorithmique, utilisation des applications, structures de données, jeux de logique, informatique et société. Il est ouvert du CM1 à la terminale, et s'adapte au niveau des élèves.

- [Algorea](https://algorea.org/#/){target="_blank"} les élèves effectuent une épreuve de 45 minutes dans une catégorie qui dépend de leur résultat lors d'un tour précédent ou du concours Castor, ce qui leur permet de faire une épreuve adaptée à leur niveau (langage Blockly, Scratch ou Python)

- [La nuit du code](https://www.nuitducode.net/){target="_blank"}

- Olympiades de NSI : les classes de quatrième (épreuve de 2 heures) et de première (épreuve de 3 heures), sous la surveillance de leur(s) enseignant(s) ; travailler en groupe autour de deux exercices originaux tout en mobilisant leurs connaissances acquises au cours de l’année. 

Académie de Normandie [2023](https://nsi-snt.ac-normandie.fr/olympiades-d-informatique-edition-2023){target="_blank"} et  [2022](https://nsi-snt.ac-normandie.fr/olympiades-d-informatique-2022-de-1ere){target="_blank"}

 Académie de Toulouse [2024](https://pedagogie.ac-toulouse.fr/informatique/olympiades-de-nsi){target="_blank"}

 Académie de La réunion [2024](https://www.ac-reunion.fr/les-olympiades-de-nsi-2024){target="_blank"}

- [Les trophées NSI](https://trophees-nsi.fr/){target="_blank"} récompenser des projets informatiques réalisés dans le cadre de l'enseignement de spécialité

- [Passe ton Hack d'abord](https://www.education.gouv.fr/passe-ton-hack-d-abord-plus-de-1-200-equipes-de-lyceens-prets-se-glisser-dans-la-peau-des-380646){target="_blank"} résoudre les différents challenges proposés et découvrir les codes de victoire ou « flag » ; avec une difficulté croissante, ces épreuves portent sur la programmation, la recherche en source ouverte (OSINT), la cryptologie, la stéganographie, l’analyse de réseaux...

- [Girls Can Code](https://girlscancode.fr/){target="_blank"} stage d'informatique pour les collégiennes et lycéennes (gratuit, sans prérequis, crétion d'un projet personnel)

- [Prologin](https://prologin.org/){target="_blank"}  concours d'informatique ouvert à tous les étudiants agés de moins de 21, pour faire découvrir le monde de la programmation et de l'algorithmique aux étudiants et de les confronter à des problèmes classiques et des challenges excitants. Le concours se déroule en trois étapes de sélections consécutives présentées dans les sections suivantes

- [Site du Concours général](https://www.education.gouv.fr/le-concours-general-des-lycees-et-des-metiers-un-prix-d-excellence-10022){target="_blank"}  distingue les meilleurs élèves des lycées ; les candidats composent sur des sujets conformes aux programmes officiels mais dans le cadre d'épreuves plus exigeantes et plus longues que celles du baccalauréat ; [Eduscol](https://eduscol.education.fr/1455/presentation-du-concours-general-des-lycees-et-des-metiers){target="_blank"} et [sujets](https://eduscol.education.fr/1443/sujets-et-rapports-de-jury-du-concours-general-des-lycees-et-des-metiers){target="_blank"}



## Concours Informatique du supérieur

- [Capes de NSI](https://capes-nsi.org/ressources/#session-2022){target="_blank"} 

- [Agrégation d'informatique](https://agreg-info.org/){target="_blank"}



