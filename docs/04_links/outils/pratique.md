title: sites pratiques

# Sites pratiques

- [Basthon](https://basthon.fr/){target="_blank"} Bac à sable pour Python, SQL, Ocaml, JS et Python, dans le navigateur.

- [Pythontutor](https://pythontutor.com/visualize.html#mode=edit){target="_blank"} Visualiser le code en Python, C, C++ et Java.


- [pythonanywhere](https://www.pythonanywhere.com//){target="_blank"} Host, run, and code Python in the cloud!

- [Brython](https://brython.info/index.html){target="_blank"} Une implémentation de Python 3 pour la programmation web côté client

- [JupyterLite](https://jupyterlite.forge.aeif.fr/lab/index.html){target="_blank"}  Une distribution de JupyterLab accessible directement dans le navigateur

- [Skult](http://skulpt.org/){target="_blank"} implementation de Python dans une page web.

- [Python/Geogebra](https://geogebra.org/python/index.html){target="_blank"} Visualiser vos programmes avec geogebra

- - - - - - - 

Programmer une application Mobile :

- [Kivy](https://kivy.org/){target="_blank"} The Open Source Python App Development Framework

- [Flet](https://flet.dev/docs/publish/web/static-website/){target="_blank"}  publishing Flet app into a standalone static website (SPA) that runs entirely in the browser with Pyodide and does not require any code running on the server side

- [Flet](https://forge.apps.education.fr/gervais/flet-build-apk-avec-docker){target="_blank"} une image Docker qui met à disposition les outils nécessaires à flet  par  Jean-Marc Gervais


- [Cordova](https://cordova.apache.org/){target="_blank"} Mobile apps with HTML, CSS & JS

- [SITULEARN](https://situlearn.univ-lemans.fr/){target="_blank"}  vise à aider les enseignants à enrichir leurs sorties pédagogiques avec des applications mobiles ;  un ensemble d’applications libres

- - - - - - - 

- [Atelier du numérique](https://www.ateliernumerique.net/){target="_blank"} Des ateliers permettent d'avoir, côte à côte, un document de travail (cours, tutoriel, exercices...) et un environnement de création (Scratch, Geogebra, BlocksCAD...).

- [Cahiers numériques](https://www.cahiernum.net/){target="_blank"}  avoir, côte à côte, un document de travail (cours, tutoriel, exercices...) et un environnement de création (Scratch, Geogebra, Basthon, BlocksCAD...)



- - - - - - - - - 

- [Fonts Google](https://fonts.google.com/){target="_blank"} 

- [Site Emoji](https://www.copyandpasteemoji.com/){target="_blank"}

- [Lorem Ipsum](https://www.faux-texte.com/){target="_blank"} Générateur de faux textes aléatoires.

- [Piskel](https://www.piskelapp.com/){target="_blank"}  Editeur en ligne pour animer des sprites et faire du pixel art.

- [Pixilart](https://www.pixilart.com/){target="_blank"}  Editeur en ligne pour faire du pixel art.
- - - - - - - -

- [Netlify](https://www.netlify.com/){target="_blank"} Déployer son site en ligne.


- [Versionnage avec GIT](http://www.silanus.fr/nsi/premiere/git/git.html){target="_blank"}

- - - - - - - -


- [ObjGen](https://beta5.objgen.com/){target="_blank"}  Live code generator pour JSON et HTML.


- [JSONLint](https://jsonlint.com/){target="_blank"} Validateur de code JSON

- - - - - - -

- [Mocodo](https://www.mocodo.net/){target="_blank"} Mocodo est un logiciel d'aide à l'enseignement et à la conception des bases de données relationnelles.

- [dbdiagram](https://dbdiagram.io/home){target="_blank"} Dessiner des diagramme relationnels pour Bases de Données

- - - - - -




- [Logic circuit](https://logic.modulo-info.ch/){target="_blank"} Crétion de circuits logiques ; mode d'emploi [Atelier simulateur logique](http://jp.pellet.name/hep/didapro9/){target="_blank"} ;  site restreint aux portes non, et, ou, ou exclusif :  [Logic Bases](https://logic.modulo-info.ch/?showonly=in,out,and,or,xor,not){target="_blank"}



- [Mermaid](https://mermaid.live/edit#pako:eNpV0E2LwyAQgOG_EmYvE0jAtvvR5rZp0v067h69SNRtwMRizaGU_vedmAqrp_HhBWWu0FmpoIJfJ07H7KfhY0bnFXGV51lZllmNuM7zOwfZI26i1EEaxMdUWsSnKPsgB8TnVN4QX6I0Qd4Rt6l8IO6itEE-6WsspS-i1UwLnv3FKHpD98ZUD5qxorPGOhq1_l_U94IxnRRzAwUMyg2il7Sb6ywc_FENikNFo1RaTMZz4OON0ukkhVet7L11UHk3qQLE5O33ZezifWmaXtCmhwVvf8nkZDk){target="_blank"} Différents diagrammes que l'on peut éditer sous différents formats, où intégrer à d'autres documents.


- - - - - - - -


- [Humanize](https://humanize.me/){target="_blank"} site web de jérémy ; on y trouve des articles sur la culture numérique, mais aussi des tutos pratiques
