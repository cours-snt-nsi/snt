title: cle bootable



Ventoy permet de créer des clés bootables:

- [Ventoy](https://www.ventoy.net/en/index.html){target="_blank"} 

Mkusb interface graphique pour dd

- [mkusb](https://doc.ubuntu-fr.org/mkusb){target="_blank"}   Dupliquer le fichier de boot ou créer une clé persistante


Freeduc-USB : de  petits objets faciles à manier, qui sont capables de se servir d'un ordinateur pour réaliser une fonction particulière

- [Freeduc-USB](https://usb.freeduc.org/){target="_blank"}  exclusivement basée sur des logiciels libres ; le média vif permet d'emporter avec soi tout ce qui compte, il s'adapte à tout ordinateur hôte


- - - - - -

Ajouter des Systèmes d'exploitation sur la clé :

- [ArchivesOS](https://archiveos.org/){target="_blank"} 

- [Tails](https://tails.boum.org/index.fr.html){target="_blank"} Un système d'exploitation portable qui protège contre la surveillance et la censure. 


- [Hirens Bootscd](https://www.tech2tech.fr/hirens-bootcd-revient-apres-6-longues-annees/){target="_blank"} Contient des logiciels gratuit ou libre,  permettant  de réparer ou diagnostiquer un système défaillant.

- - - - - -

Installer des logiciels sur la clé pour les avoir à disposition :

- [Navigateur Thor](https://www.torproject.org/fr/){target="_blank"} Naviguer sur Internet en toute confidentialité.

- [portablesapps](https://portableapps.com/fr){target="_blank"}  Permet d'accéder à de nombreuses applications portables.

- [libreelec](https://libreelec.tv/){target="_blank"} Bibliothèque multimedia nomade.

- [bitwarden](https://bitwarden.com/){target="_blank"} Gestionnaire de mots de passe.

- [malwarebytes](https://fr.malwarebytes.com/){target="_blank"} Lutter contre les malwares.

- - - - - -

Avoir une console de rétrogaming sur clé :

- [Batocera](https://batocera.org/){target="_blank"} Emulateur de vieilles consoles.

- [Retrostic](https://www.retrostic.com/){target="_blank"} Site ou l'on peut télécharger des jeux.

- [Retrogaming](https://www.rom-game.fr/recherche-retrogaming.html){target="_blank"} 