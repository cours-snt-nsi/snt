title: cours et exercices

# Des sites proposant des exercices pour s'entrainer


## Langage Python

- [CodEx](https://codex.forge.apps.education.fr/){target="_blank"} exercices d'apprentissage de l'algorithmique et de la programmation par des professeurs d'informatique du secondaire et du supérieur

- [e-nsi ressources](https://e-nsi.forge.aeif.fr/){target="_blank"}  des professeurs d'informatique du secondaire et du supérieur, qui produisent collectivement des ressources pédagogiques sur le thème des sciences informatiques

- [Structures arborescentes](https://pratique.forge.apps.education.fr/arbre/){target="_blank"} par Franck CHAMBON : approfondir les connaissances en algorithmique et se tester en autonomie

- [Dépôt](https://forge.apps.education.fr/nreveret){target="_blank"}  de Nicolas Reveret, donnant accès à des sites pour les élèves, sur les tris, les dicitonnaires, les données en tables, les bases de données... 

- [Le pyvert](https://diraison.github.io/Pyvert/){target="_blank"} site de Jean Diraison ; 128 exercices de base d'entraînement à la programmation


- [HackInScience](https://www.hackinscience.org/exercises/){target="_blank"} Exercices auto-corrigés par Julien Palard

- [GenPyExo](https://diraison.github.io/GenPyExo/){target="_blank"} Exercices auto-corrigés par Jean Diraison

 - - - - - - - - -

## Langage SQL

- [Site](https://nreveret.forge.apps.education.fr/exercices_bdd/){target="_blank"} différents exercices sur les bases de donnée et le langage SQL
par Nicolas Reveret

- [Site](https://colibri.unistra.fr/fr/course/list/notions-de-base-en-sql){target="_blank"} Notions de base en SQL  par Antoine Moulin et David Cazier
