title: apprendre/jouer

## Apprendre à coder en jouant, en résolvant des puzzles, des énigmes...


- [Py-rates](https://py-rates.fr/){target="_blank"} L’apprentissage de la programmation informatique en Python.



- [FuturCoder](https://futurecoder.forge.aeif.fr/){target="_blank"} Tutoriel de programmation interactif pour apprendre à programmer en Python

- [France IOI](http://www.france-ioi.org/algo/index.php){target="_blank"}  Site d'entraînement permetant de progresser rapidement en programmation et en algorithmique, par un apprentissage allant de la découverte des bases de la programmation jusqu'au niveau des compétitions internationales les plus prestigieuses, en Python, C, C++, JavaScool, Java, OCaml ou Pascal. 


- [Codingame](https://www.codingame.com/start){target="_blank"} Améliorez vos compétences en programmation.




- - - - - - 

En Anglais

- [Advent of code](https://adventofcode.com/){target="_blank"} Un calendrier de l'avent avec de petits puzzles de programmation.

- [Codewars](https://www.codewars.com/){target="_blank"} Ameliorer ses compétences en codage en s'entrainant.

- [Python Challenge](http://www.pythonchallenge.com/about.php){target="_blank"} Un jeu dont chaque niveau peut être résolu par un petit programme.

- [RealPython](https://realpython.com/){target="_blank"} Tutoriels pour apprendre Python

- [Checkio](https://py.checkio.org/){target="_blank"} Des puzzles.

- [Robozzle](http://www.robozzle.com/beta/index.html){target="_blank"} a social puzzle game.


- [Projet Euler](https://projecteuler.net/){target="_blank"} a series of challenging mathematical/computer programming problems

- [Sphere Online Judge](https://www.spoj.com/problems/classical/sort=6){target="_blank"} Banque d'exercices

- [Hacker Rank](https://www.hackerrank.com/domains/python){target="_blank"} source d'idées d'activités 

- [Leet Code](https://leetcode.com/){target="_blank"} source d'idées d'activités 






- - - - - - - 

- [Jeux Terminus](http://luffah.xyz/bidules/Terminus/){target="_blank"} Jeu permettant de découvrir les commandes Bash.


- [SQL Murder Mystery by NUKnightLab ](https://mystery.knightlab.com/){target="_blank"} et [SQL Murder](https://edenmaths.gitlab.io/bcpst1/2022_23/INFORMATIQUE/12_sql/#sql-murder-mystery){target="_blank"}  version intégrée sur le site de Guillaume Connan : un meurtre a été commis dans la ville de SQL City. Il est temps d'enquêter !

- [SQL Island](https://sql-island.informatik.uni-kl.de/?lang=fr){target="_blank"} Après avoir survécu à un accident d'avion nous voilà coincé sur SQL Island ! Maintenant il faut trouver un moyen de t'échapper de cette île.

- [Cube composer](https://david-peter.de/cube-composer/){target="_blank"} Puzzle avec des cubes, pour créer des formes en utilisant la programmation fonctionnelle.

- [Cryptopals](https://cryptopals.com/){target="_blank"} a collection of exercises that demonstrate attacks on real-world crypto

- [BlockChainBattle](https://e-nsi.forge.aeif.fr/blockly-games/fr/index.html){target="_blank"} Découvrir les principes de la blockchain via un jeu vidéo.
- - - - - - -

- [PIX](https://pix.fr/enseignement-scolaire/){target="_blank"} Développer et certifier ses compétences numériques.

- [Blocky Games](https://blockly.games/?lang=fr){target="_blank"} Jeux pour les programmeurs de demain. 

- [Toxicode](http://www.toxicode.fr/learn){target="_blank"} outils ludiques pour faire développer la programmation 

- [Studio Code](https://studio.code.org/courses?lang=fr-FR){target="_blank"} Cours d'apprentissage de l'informatique pour les élèves


- [Prisonnier Quantique](https://prisonnier-quantique.fr/index.html){target="_blank"} Un jeu vidéo gratuit  pour diffuser la culture scientifique et technique.

- [CargotBot](http://www-verimag.imag.fr/~wack/CargoBot/){target="_blank"} de Rui Viana.



## Site pour créer des jeux

- [Site Unity](https://unity.com/fr){target="_blank"}


- [Site MicroStudio](https://microstudio.dev/fr/){target="_blank"}  un moteur de jeu gratuit, en ligne, créé par Gilles Pommereuil ([Github](https://github.com/pmgl/microstudio){target="_blank"}) et [Tutoriels en Python par Teddy CHENE](https://gitlab.com/TeddyChene/microstudio){target="_blank"}


- [Godot](https://godotengine.org/){target="_blank"} Moteur de jeu

- [Renpy](https://www.renpy.org/){target="_blank"} un outil auteur permettant de créer des jeux-vidéo de type Visual novel





