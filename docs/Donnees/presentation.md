---
author: à compléter
title: 📋 Les données structurées et leur traitement
---

!!! abstract "Présentation"

    Les données constituent la matière première de toute activité numérique. Afin de permettre leur réutilisation, il est nécessaire de les conserver de manière persistante. Les structurer correctement garantit que l’on puisse les exploiter facilement pour produire de l’information. Cependant, les données non structurées peuvent aussi être exploitées, par exemple par les moteurs de recherche.

!!! note "01. Video Introduction Données"

	<div class="centre">
	<iframe width="1280" height="720" src="https://ladigitale.dev/digiview/#/v/66b0d0b282864" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
	</div>



|Contenus|Capacités attendues|
|:--- | :--- |
|Données |Définir une donnée personnelle. Identifier les principaux formats et représentations de données.|
|Données structurées|Identifier les différents descripteurs d'un objet. Distinguer la valeur d'une donnée de son descripteur Utiliser un site de données ouvertes, pour sélectionner et récupérer des données.|
|Traitement de données structurées|Réaliser des opérations de recherche, filtre, tri ou calcul sur une ou plusieurs tables.|
|Métadonnées|Retrouver les métadonnées d'un fichier personnel.|
|Données dans le nuage *(cloud)*|Utiliser un support de stockage dans le nuage. Partager des fichiers, paramétrer des modes de synchronisation. Identifier les principales causes de la consommation énergétique des centres de données ainsi que leur ordre de grandeur.|
