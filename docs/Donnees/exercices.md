---
author: Steeve PYTEL
title: 🏢 Exercices
---

# Exercices

!!! abstract "Activitées Pixel'Art"

    1. [Easy](https://capytale2.ac-paris.fr/web/c/dbee-644319){target='_blank'}
    2. [Drapeau](https://capytale2.ac-paris.fr/web/c/125f-1447786){target='_blank'}
    3. [Zig](https://capytale2.ac-paris.fr/web/c/4cc1-567778){target='_blank'}
    4. [Echec](https://capytale2.ac-paris.fr/web/c/ab28-802220){target='_blank'}

!!! abstract "Débuter avec les Outils en ligne"

    1. [Boucles](https://capytale2.ac-paris.fr/web/c/073b-742357){target='_blank'}
    2. [Si](https://capytale2.ac-paris.fr/web/c/6961-922493){target='_blank'}  
    3. [While](https://capytale2.ac-paris.fr/web/c/0285-1279188){target='_blank'}
    4. [Dessin](https://capytale2.ac-paris.fr/web/c/0285-1279188){target='_blank'}
    5. [Mélange](https://capytale2.ac-paris.fr/web/c/0285-1279188){target='_blank'}
    6. [Découverte Python](https://capytale2.ac-paris.fr/web/c/ca10-632362){target='_blank'}

!!! abstract "Capytale mode expert"

    1. [Coloriage](https://capytale2.ac-paris.fr/web/c/073e-1952982){target='_blank'}
    2. [Manipulation](https://capytale2.ac-paris.fr/web/c/6271-3574308){target='_blank'}
    3. [Cache-cache](https://capytale2.ac-paris.fr/web/c/9cdd-3700092){target='_blank'}
    4. [Cache-Console](https://capytale2.ac-paris.fr/web/c/6218-1490234){target='_blank'}