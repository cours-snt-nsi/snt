---
author: Steeve PYTEL
title: 🏢 Exercices
---

# Exercices

!!! abstract "Capytale"

    1. [Microbit](https://capytale2.ac-paris.fr/web/c/c2d5-1767097){target='_blank'}


!!! abstract "Projets"

    - DM sur le freinage ABS : [pdf](a_telecharger/projets/freinage_ABS.pdf) ou [odt](a_telecharger/projets/freinage_ABS.odt)
    ??? note "01. DM sur le freinage ABS "

        <div class="centre">
        <iframe 
        src="../a_telecharger/projets/freinage_ABS.pdf"
        width="1000" height="1000" 
        frameborder="0" 
        allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
        </iframe>
        </div>
    - Réalisation d'un système embarqué : [pdf](a_telecharger/projets/un_tamagotchi.pdf) ou [odt](a_telecharger/projets/un_tamagotchi.odt)
    ??? note "02. Réalisation d'un système embarqué "

        <div class="centre">
        <iframe 
        src="../a_telecharger/projets/un_tamagotchi.pdf"
        width="1000" height="1000" 
        frameborder="0" 
        allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
        </iframe>
        </div>
