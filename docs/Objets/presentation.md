---
author: à compléter
title: 📋 Informatique embarquée et objets connectés
---

!!! abstract "Présentation"

    Embarquer l’informatique dans les objets a beaucoup d’avantages : simplifier leur fonctionnement, leur donner plus de possibilités d’usage et de sûreté, et leur permettre d’intégrer de nouvelles possibilités à matériel constant par simple modification de leur logiciel.
    Après avoir transformé les chaînes de montage des automobiles et les avions dans les années quatre-vingt-dix, l’informatique intervient maintenant dans des domaines toujours plus nombreux : automobile, réseau ferroviaire et transports urbains, domotique, robotique, loisirs, etc., conduisant à un nouvel internet des objets.
    Pour les avions par exemple, l’informatique gère le vol en commandant finement des servomoteurs électriques, plus légers et plus fiables que les vérins hydrauliques, les réacteurs, la navigation et le pilotage automatique, et permet l’atterrissage automatique par temps de brouillard. Elle a eu un impact décisif sur l’amélioration de la sécurité aérienne.
    Les objets informatisés avaient autrefois des interfaces homme-machine (IHM) dédiées, souvent dépendantes d’une liaison filaire directe. Mais les technologies du Web intégrées au téléphone portable permettent maintenant d’y rassembler les interfaces des objets du quotidien, ce qui en simplifie et uniformise l’usage. Les objets informatisés deviennent ainsi connectés.

!!! note "01. Video Introduction aux objets connectés"

	<div class="centre">
	<iframe width="1280" height="720" src="https://ladigitale.dev/digiview/#/v/66b0c70e1e2d6" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
	</div>

|Contenus|Capacités attendues|
|:--- | :--- |
|Systèmes informatiques embarqués|Identifier des algorithmes de contrôle des comportements physiques à travers les données des capteurs, l'IHM et les actions des actionneurs dans des systèmes courants.|
|Interface homme-machine (IHM)|Réaliser une IHM simple d'un objet connecté.|
|Commande d'un actionneur, acquisition des données d'un capteur|Écrire des programmes simples d'acquisition de données ou de commande d'un actionneur.|
