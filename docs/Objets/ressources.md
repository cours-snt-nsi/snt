---
author: à compléter
title: 📚 Ressources
---

# Informatique embarquée et objets connectés



## Introduction aux objets connectés

!!! abstract "Présentation"

    Dans cette activité, les élèves visionnent une vidéo d’introduction aux objets connectés et rédigent un texte pour résumer ce qu’ils ont compris.



??? note "01. Introduction aux objets connectés"

	<div class="centre">
	<iframe 
	src="../a_telecharger/01/01. Introduction aux objets connectés - OBJETS CONNECTES.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>



[Ressources 01. Introduction aux objets connectés](https://forge.apps.education.fr/ressources/snt/-/tree/main/docs/Objets/a_telecharger/01){ .md-button target="_blank" rel="noopener" }


## Repères historiques

!!! abstract "Présentation"

    Dans cette activité, les élèves vont découvrir les dates clés en lien avec les objets connectés et les situer dans un contexte historique plus large.
    Ils créeront une frise chronologique et des supports de révision.



??? note "02. Repères historiques"

	<div class="centre">
	<iframe 
	src="../a_telecharger/02/02. Repères historiques  - OBJETS CONNECTES.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>



[Ressources 02. Repères historiques](https://forge.apps.education.fr/ressources/snt/-/tree/main/docs/Objets/a_telecharger/02){ .md-button target="_blank" rel="noopener" }


## Programmez vos objets connectés

!!! abstract "Présentation"

    Dans cette activité, on programme des objets connectés dans son navigateur, sans installation ni matériel. Débutants ou déjà initiés, les élèves manipulent en autonomie des capteurs de distance, de luminosité, un accéléromètre, un buzzer, un servomoteur, etc. tout en apprenant les fondamentaux de la programmation dans un langage au choix : Scratch, Blockly ou Python.



??? note "03. Programmez vos objets connectés"

	<div class="centre">
	<iframe 
	src="../a_telecharger/03/03. Programmez vos objets connectés - OBJETS CONNECTES.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>



[Ressources 03. Programmez vos objets connectés](https://forge.apps.education.fr/ressources/snt/-/tree/main/docs/Objets/a_telecharger/03){ .md-button target="_blank" rel="noopener" }


## Les voitures autonomes

!!! abstract "Présentation"

    Dans cette activité, on présente ce qu’il y a sous le capot d’une voiture autonome pour mieux comprendre comment fonctionnent ses nombreux capteurs : caméras, radars, lidars, ultrasons...



??? note "04. Les voitures autonomes"

	<div class="centre">
	<iframe 
	src="../a_telecharger/04/04. Les voitures autonomes - OBJETS CONNECTES.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>



[Ressources 04. Les voitures autonomes](https://forge.apps.education.fr/ressources/snt/-/tree/main/docs/Objets/a_telecharger/04){ .md-button target="_blank" rel="noopener" }


##  Les Robots

!!! abstract "Présentation"

    Dans cette activité, on présente ce que sont les robots et on s’interroge sur les conséquences de leur introduction massive sur le marché du travail. Les élèves effectueront des recherches et prépareront un débat.



??? note "05. Les Robots"

	<div class="centre">
	<iframe 
	src="../a_telecharger/05/05. Les Robots - OBJETS CONNECTES.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>



[Ressources 05. Les Robots](https://forge.apps.education.fr/ressources/snt/-/tree/main/docs/Objets/a_telecharger/05){ .md-button target="_blank" rel="noopener" }


##  Microbit N1-N2-N3-N4

!!! abstract "Présentation"

    Au travers des 4 parcours, les élèves découvrent la carte micro:bit et ses spécificités techniques. Chaque parcours se complexifie et utilise de nouveaux capteurs.



??? note "06. Microbit N1-N2-N3-N4"

	<div class="centre">
	<iframe 
	src="../a_telecharger/06/06. Microbit N1-N2-N3-N4 - OBJETS CONNECTES.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>



[Ressources 06. Microbit N1-N2-N3-N4](https://forge.apps.education.fr/ressources/snt/-/tree/main/docs/Objets/a_telecharger/06){ .md-button target="_blank" rel="noopener" }



##  Les interfaces homme-machine

!!! abstract "Présentation"

    Dans cette activité, on présente ce qu’est une interface homme-machine (IHM). En particulier, on donne quelques exemples d’IHM, puis on montre la difficulté à développer des IHM efficaces.



??? note "07. Les interfaces homme-machine"

	<div class="centre">
	<iframe 
	src="../a_telecharger/07/07. Les interfaces homme-machine  - OBJETS CONNECTES.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>



[Ressources 07. Les interfaces homme-machine](https://forge.apps.education.fr/ressources/snt/-/tree/main/docs/Objets/a_telecharger/07){ .md-button target="_blank" rel="noopener" }


##  Réalisation IHM

!!! abstract "Présentation"

    Cette activité porte sur la réalisation d’une IHM simple d’un objet connecté comme le téléphone.



??? note "08. Réalisation IHM"

	<div class="centre">
	<iframe 
	src="../a_telecharger/08/08. Réalisation IHM -  OBJETS CONNECTES.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>



[Ressources 08. Réalisation IHM](https://forge.apps.education.fr/ressources/snt/-/tree/main/docs/Objets/a_telecharger/08){ .md-button target="_blank" rel="noopener" }


##  Impact et importance de l'informatisation

!!! abstract "Présentation"

    L’activité porte sur l’impact et l’importance de l’informatisation des objets. On y aborde notamment l’exemple de la voiture qui utilise des techniques de systèmes embarqués pour son fonctionnement et sa navigation.


??? note "09. Impact et importance de l'informatisation"

	<div class="centre">
	<iframe 
	src="../a_telecharger/09/09. Impact et Importance de l'Informatisation  - OBJETS CONNECTES.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>



[Ressources 09. Impact et importance de l'informatisation](https://forge.apps.education.fr/ressources/snt/-/tree/main/docs/Objets/a_telecharger/09){ .md-button target="_blank" rel="noopener" }



