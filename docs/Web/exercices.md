---
author: Steeve PYTEL
title: 🏢 Exercices
---

# Exercices

!!! abstract "Capytale"

    1. [page vide](https://capytale2.ac-paris.fr/web/c/910d-980620){target='_blank'}
    2. [Jeu](https://capytale2.ac-paris.fr/web/c/59e2-2069430){target='_blank'}
    3. [Html 1/2](https://capytale2.ac-paris.fr/web/c/60a6-2549442){target='_blank'}
    4. [Html 2/2](https://capytale2.ac-paris.fr/web/c/fdad-2518372){target='_blank'}
    5. [Pomme Poire](https://capytale2.ac-paris.fr/web/c/ee80-936514){target='_blank'}