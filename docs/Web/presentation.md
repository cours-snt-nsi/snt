---
author: à compléter
title: 📋 Le Web
---

!!! abstract "Présentation Résumé"

    Le Web (toile ou réseau) désigne un système donnant accès à un ensemble de données (page, image, son, vidéo) reliées par des liens hypertextes et accessibles sur le réseau internet.

!!! note "01. Video Introduction WEB"

	<div class="centre">
	<iframe width="1280" height="720" src="https://ladigitale.dev/digiview/#/v/66b0d136b41e1" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
	</div>

|Contenus|Capacités attendues|
|:--- | :--- |
|Repères historiques|Connaître les étapes du développement du Web.|
|Notions juridiques|Connaître certaines notions juridiques (licence, droit d'auteur, droit d'usage, valeur d'un bien).|
|Hypertexte|Maîtriser les renvois d'un texte à différents contenus.|
|Langages HTML et CSS|Distinguer ce qui relève du contenu d'une page et de son style de présentation. Etudier et modifier une page HTML simple.|
|URL|Décomposer l'URL d'une page.|
|Requête HTTP|Reconnaître les pages sécurisées. Décomposer le contenu d'une requête HTTP et identifier les paramètres passés.|
|Modèle client/serveur|Inspecter le code d'une page hébergée par un serveur et distinguer ce qui est exécuté par le client et par le serveur. |
|Moteurs de recherche : principes et usages|Mener une analyse critique des résultats fournis par un moteur de recherche. Comprendre les enjeux de la publication d'informations.|
|Paramètres de sécurité d'un navigateur|Maîtriser les réglages les plus importants concernant la gestion des cookies, la sécurité et la confidentialité d'un navigateur. Sécuriser sa navigation en ligne et analyser les pages et fichiers.|


 
 