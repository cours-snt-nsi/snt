---
author: Steeve PYTEL
title: 🏢 Exercices
---

# Exercices

!!! abstract "Capytale"

    1. [EXIF 1](https://capytale2.ac-paris.fr/web/c/e0b9-1658411){target='_blank'}
    2. [EXIF 2](https://capytale2.ac-paris.fr/web/c/339d-1658359){target='_blank'}
    3. [EXIF 3](https://capytale2.ac-paris.fr/web/c/23a6-1489513){target='_blank'}
