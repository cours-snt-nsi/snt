---
author: à compléter
title: 📚 Ressources
---

# Localisation

## Introduction à la géolocalisation

!!! abstract "Présentation"

    Dans cette activité, les élèves visionnent une vidéo d’introduction à la géolocalisation et rédigent un texte pour résumer ce qu’ils ont compris.



??? note "01. Introduction à la géolocalisation"

	<div class="centre">
	<iframe 
	src="../a_telecharger/01/01. Introduction à la géolocalisation - LOCALISATION CARTOGRAPHIE MOBILITE.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>



[01. Introduction à la géolocalisation](https://forge.apps.education.fr/ressources/snt/-/tree/main/docs/Localisation/a_telecharger/01){ .md-button target="_blank" rel="noopener" }


## Découverte de la thématique Géolocalisation

!!! abstract "Présentation"

    Dans cette activité, on découvre la thématique à travers une vidéo et un QCM.


??? note "02. Découverte de la thématique Géolocalisation"

	<div class="centre">
	<iframe 
	src="../a_telecharger/02/02. Découverte de la thématique Géolocalisation - LOCALISATION CARTOGRAPHIE MOBILITE.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>

[02. Découverte de la thématique Géolocalisation](https://forge.apps.education.fr/ressources/snt/-/tree/main/docs/Localisation/a_telecharger/02){ .md-button target="_blank" rel="noopener" }

## Repères historiques

!!! abstract "Présentation"

    Dans cette activité, les élèves vont découvrir les dates clés en lien avec la géolocalisation et la cartographie et les situer dans un contexte historique plus large.
	Ils créeront une frise chronologique et des supports de révision.


??? note "03. Repères historiques"

	<div class="centre">
	<iframe 
	src="../a_telecharger/03/03. Repères historiques - LOCALISATION CARTOGRAPHIE MOBILITE.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>

[03. Repères historiques](https://forge.apps.education.fr/ressources/snt/-/tree/main/docs/Localisation/a_telecharger/03){ .md-button target="_blank" rel="noopener" }


## Principe de fonctionnement de la géolocalisation

!!! abstract "Présentation"

    Dans cette activité, on présente le principe de fonctionnement de la géolocalisation à partir de vidéos. Les 
	élèves apprennent à manipuler une carte numérique à l'aide du site collaboratif Géoportail.


??? note "04. Principe de fonctionnement de la géolocalisation"

	<div class="centre">
	<iframe 
	src="../a_telecharger/04/04. Principe de fonctionnement de la géolocalisation - LOCALISATION CARTOGRAPHIE MOBILITE.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>


[04. Principe de fonctionnement de la géolocalisation](https://forge.apps.education.fr/ressources/snt/-/tree/main/docs/Localisation/a_telecharger/04){ .md-button target="_blank" rel="noopener" }


## Comprendre le principe de la géolocalisation satellite

!!! abstract "Présentation"

    Dans cette activité, les élèves comprennent le principe de géolocalisation par satellite à l’aide d’activités débranchées.


??? note "05. Comprendre le principe de la géolocalisation satellite"

	<div class="centre">
	<iframe 
	src="../a_telecharger/05/05. Comment marche le GPS - LOCALISATION CARTOGRAPHIE MOBILITE.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>

[05. Comprendre le principe de la géolocalisation satellite](https://forge.apps.education.fr/ressources/snt/-/tree/main/docs/Localisation/a_telecharger/05){ .md-button target="_blank" rel="noopener" }


## GPS et Galiléo

!!! abstract "Présentation"

    Dans cette activité en ligne, les élèves manipulent un globe terrestre pour comprendre les notions de latitude, longitude et trilatération.


??? note "06. GPS et Galiléo"

	<div class="centre">
	<iframe 
	src="../a_telecharger/06/06. GPS et Galiléo- LOCALISATION CARTOGRAPHIE MOBILITE.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>

[06. GPS et Galiléo](https://forge.apps.education.fr/ressources/snt/-/tree/main/docs/Localisation/a_telecharger/06){ .md-button target="_blank" rel="noopener" }



## Traitement de données

!!! abstract "Présentation"

    Dans cette activité, on utilise des données ouvertes tirées de data.gouv.fr et on les traite avec les élèves, d’une part avec un tableur
	 et d’autre part avec des programmes Python pour extraire et afficher des données pertinentes.


??? note "07. Traitement de données"

	<div class="centre">
	<iframe 
	src="../a_telecharger/07/07. Traitement et affichage de données - LOCALISATION CARTOGRAPHIE MOBILITE.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>

[07. Traitement de données](https://forge.apps.education.fr/ressources/snt/-/tree/main/docs/Localisation/a_telecharger/07){ .md-button target="_blank" rel="noopener" }



## Cartes numériques

!!! abstract "Présentation"

    Dans cette activité, les élèves apprennent à utiliser une carte numérique. On introduit la notion d’échelle, la possibilité d’ajouter des données 
	sur la carte numérique comme des points nommés, des lignes et des surfaces pour délimiter des zones.


??? note "08. Cartes Numériques"

	<div class="centre">
	<iframe 
	src="../a_telecharger/08/08. Cartes Numériques - LOCALISATION CARTOGRAPHIE MOBILITE.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>

[08. Cartes Numériques](https://forge.apps.education.fr/ressources/snt/-/tree/main/docs/Localisation/a_telecharger/08){ .md-button target="_blank" rel="noopener" }



## Calculs d'itinéraires

!!! abstract "Présentation"

    Dans cette activité, les élèves apprennent à représenter un réseau routier sous la forme d’un graphe puis trouver le plus court chemin dans ce graphe.


??? note "09. Calculs d'itinéraires"

	<div class="centre">
	<iframe 
	src="../a_telecharger/09/09. Calculs d_itinéraires - LOCALISATION CARTOGRAPHIE MOBILITE.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>

[09. Calculs d_itinéraires](https://forge.apps.education.fr/ressources/snt/-/tree/main/docs/Localisation/a_telecharger/09){ .md-button target="_blank" rel="noopener" }




## Exploiter des sismogrammes

!!! abstract "Présentation"

    La géolocalisation GPS s’appuie sur la triangulation, il est donc tout à fait justifié de s’appuyer sur une activité où l’élève 
	devra localiser par triangulation un épicentre, à partir du temps mis par les ondes P pour arriver à 3 stations.

??? note "10. Exploiter des sismogrammes"

	<div class="centre">
	<iframe 
	src="../a_telecharger/10/10. Exploiter des sismogrammes - LOCALISATION CARTOGRAPHIE MOBILITE.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>

[10. Exploiter des sismogrammes](https://forge.apps.education.fr/ressources/snt/-/tree/main/docs/Localisation/a_telecharger/10){ .md-button target="_blank" rel="noopener" }

