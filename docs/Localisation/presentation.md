---
author: à compléter
title: 📋 Localisation, cartographie et mobilité
---

!!! abstract "Présentation"

    La cartographie est essentielle pour beaucoup d’activités : agriculture, urbanisme, transports, loisirs, etc. Elle a été révolutionnée par l’arrivée des cartes numériques accessibles depuis les ordinateurs, tablettes et téléphones, bien plus souples à l’usage que les cartes papier.
    Les cartes numériques rassemblent toutes les échelles et permettent de montrer différents aspects de la région visualisée sur une seule carte. Les algorithmes de recherche permettent de retrouver sur la carte les endroits en donnant simplement leur nom, et de calculer des itinéraires entre points selon des modes de transports variés. 

!!! note "01. Video Introduction GPS"

	<div class="centre">
	<iframe width="1280" height="720" src="https://ladigitale.dev/digiview/#/v/66b0d1dceeb3d" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
	</div>


|Contenus|Capacités attendues|
|:--- | :--- |
|GPS, Galileo|Décrire le principe de fonctionnement de la géolocalisation.|
|Cartes numériques|Identifier les différentes couches d'information de GeoPortail pour extraire différents types de données. Contribuer à OpenStreetMap de façon collaborative.|
|Protocole NMEA 0183|Décoder une trame NMEA pour trouver des coordonnées géographiques.|
|Calculs d'itinéraires|Utiliser un logiciel pour calculer un itinéraire. Représenter un calcul d'itinéraire comme un problème sur un graphe.|
|Confidentialité|Régler les paramètres de confidentialité d'un téléphone pour partager ou non sa position.|
