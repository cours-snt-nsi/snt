---
author: Steeve PYTEL
title: 🏢 Exercices
---

# Exercices

!!! abstract "Capytale"

    1. [Graphes 1/2](https://capytale2.ac-paris.fr/web/c/cc08-3406821){target='_blank'}
    2. [Graphes 2/2](https://capytale2.ac-paris.fr/web/c/9dcd-3133354){target='_blank'}