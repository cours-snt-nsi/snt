---
author: à compléter
title: 📋 Les réseaux sociaux
---

!!! abstract "Présentation Résumé"

    Les réseaux sociaux sont des applications basées sur les technologies du Web qui offrent un service de mise en relation d’internautes pour ainsi développer des communautés d’intérêts.

!!! note "01. Video Introduction Réseaux Sociaux"

	<div class="centre">
	<iframe width="1280" height="720" src="https://ladigitale.dev/digiview/#/v/66b0d170c4458" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
	</div>

|Contenus|Capacités attendues|
|:--- | :--- |
|Identité numérique, e-réputation, identification, authentification|Connaître les principaux concepts liés à l'usage des réseaux sociaux.|
|Réseaux sociaux existants|Distinguer plusieurs réseaux sociaux selon leurs caracteristiques, y compris un ordre de grandeur de leurs nombres d'abonnés. Paramétrer des abonnements pour assurer la confidentialité de données personnelles.|
|Modèle économique des réseaux sociaux|Identifier les sources de revenus des entreprises de réseautage social.|
|Rayon, diamètre et centre d'un graphe|Déterminer ces caractéristiques sur des graphes simples.|
|Notion de « petit monde ». Expérience de Milgram|Décrire comment l'information présentée par les réseaux sociaux est conditionnée par le choix préalable de ses amis.|
|Cyberviolence|Connaître les dispositions de l'article 222-33-2-2 du code pénal. Connaître les différentes formes de cyberviolence (harcèlement, discrimination, sexting...) et les ressources disponibles pour lutter contre la cyberviolence.|

    




