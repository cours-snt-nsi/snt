const SELECTED_CLASS = "selected-tag";


function gestion_tableau() {
    let and_mode = true;
    let selected_tags = {};
    let affichage_travaux = true;

    $('#tableau-recherche thead tr').clone(true).addClass('filters').appendTo('#tableau-recherche thead');

    // We overwrite html-comparisons functions
    // in order to deal with french accentuation
    var collator = new window.Intl.Collator('fr');
    var types = $.fn.dataTable.ext.type;
    delete types.order['string-pre'];
    types.order['html-asc'] = collator.compare;
    types.order['html-desc'] = function (a, b) {
        return collator.compare(a, b) * -1;
    };

    let table = $('#tableau-recherche').DataTable({
        orderCellsTop: true,
        pageLength: 50,
        order: [[5, 'asc'], [0, 'asc']],
        columnDefs: [                                           // See mkdocs_macros_pyodide/macros_recherche.py for table construction
            { target: 0, searchable: true, type: 'html' },      // Exercise's name
            { target: 1, orderData: [4, 1], searchable: true }, // Difficulty class as a string
            { target: 2, searchable: true, type: 'string' },    // Tags
            { target: 3, orderData: [6, 3], searchable: false },// Date of "index.md" last update
            { target: 4, visible: false, searchable: false },   // Difficulty score (e.g. 147)
            { target: 5, visible: false, searchable: false },   // Difficulty class as an integer (e.g. 1 = score // 100)
            { target: 6, visible: false, searchable: false },   // Timestamp of "index.md" last update
        ],
        lengthMenu: [[25, 50, 100, -1], [25, 50, 100, "Tous"]],  // [[options...], [string...]] for number of shown exercises dropdown menu
        layout: {
            topStart: '',
            topEnd: '',
            bottomStart: 'info',
            bottomEnd: 'paging',
            bottom2Start: 'pageLength',
        },
        initComplete: function () {
            var api = this.api();

            var cell = $('.filters th').eq(
                $(api.column(0).header()).index()
            );

            $(cell).html(`
            <input class="recherche" id="recherche-nom" type="search" size="30" placeholder="Filtrer par titre d&apos;exercice" />
            <input id="reset-recherche-nom" type="reset" value="x" alt="Effacer" onclick="vide()">
            `);

            // On every keypress in this input
            $(
                'input',
                $('.filters th').eq($(api.column(0).header()).index())
            )
                .on('keyup change', function (e) {
                    $(this).attr('title', $(this).val());
                    var regexr = '({search})';

                    api.column(0).search(
                        this.value != ''
                            ? regexr.replace('{search}', '(((' + this.value + ')))')
                            : '',
                        this.value != '',
                        this.value == ''
                    ).draw();
                });
        },
        language: {
            "decimal": "",
            "emptyTable": "Aucune ligne trouvée",
            "info": "Lignes _START_ à _END_ parmi _TOTAL_",
            "infoEmpty": "Aucune ligne à montrer",
            "infoFiltered": "(filtrées parmi _MAX_ lignes)",
            "infoPostFix": "",
            "thousands": ",",
            "lengthMenu": "Afficher _MENU_ lignes",
            "loadingRecords": "Chargement...",
            "processing": "",
            "search": "Chercher :",
            "zeroRecords": "Aucune ligne correspondante",
            "paginate": {
                "first": "Premier",
                "last": "Dernier",
                "next": "Suivant",
                "previous": "Précédent"
            },
            "aria": {
                "sortAscending": ": cliquer pour trier la colonne dans l'ordre croissant",
                "sortDescending": ": cliquer pour trier la colonne dans l'ordre décroissant"
            }
        },
    });

    var collator = new Intl.Collator('fr'); // tri lexicographique tenant compte des accents
    table.sort(collator.compare);

    // Sélection de la difficulté
    // ajout d'un select
    var colonne = table.columns(1);
    var cell = $('.filters th').eq($(colonne.header()).index());
    $(cell).html('');
    var select = $('<select></select>').appendTo($(cell)).on('change', function () {
        colonne.search($(this).val()).draw();
    });

    // récupération des difficultés uniques
    var difficultes = table.column(1).data().unique();

    // ajout des options de difficultés
    select.append($('<option class="vide" value="" selected="selected">Toutes les difficultés</option>'));
    $.each(difficultes, function (idx, diff) {
        var liste_classes = $(diff).attr("class").split(/\s+/);
        var classe = liste_classes[liste_classes.length - 1];
        var texte = $(diff).contents()[0].nodeValue;
        select.append($('<option class="' + classe + '" value="' + texte + '">' + texte + '</option>'));
    });

    // Suppression de l'entête de la colonne "Mise à jour"
    $('.filters th').eq($(table.columns(3).header()).index()).html('');

    // Positionnement des boutons de tri
    for (let i = 0; i < 4; i++) {
        let arrows = $('.dt-column-order').eq($(table.columns(i).header()).index());
        let title = $('.dt-column-title').eq($(table.columns(i).header()).index());
        title.next('span').remove();
        let row = $('th.dt-orderable-asc').eq($(table.columns(i).header()).index());
        row.prepend(arrows);
    }

    // Affichage des exercices en travaux
    $('.filters th').eq($(table.columns(2).header()).index()).html('');
    $('#label-travaux').on('click', function () {
        table.columns(0).search('.*', true, false).draw();
        affichage_travaux = !affichage_travaux;
        if (affichage_travaux) {
            // affichage_travaux = true;
            $('#span-travaux').css('opacity', 1);
            table.columns(2).search('.*', true, false).draw();
        } else {
            // affichage_travaux = false;
            $('#span-travaux').css('opacity', 0.6);
            table.columns(2).search('^(?!.*en travaux).*$', true, false).draw();
        }
        table.columns(0).search($('#recherche-nom').val(), true, false).draw();
    })


    /* Custom search function used for filtering tags each times table is drawn
    * Returns true if row is shown, false otherwise
    */
    $.fn.dataTable.ext.search.push(
        function (settings, searchData) {
            // Filter is active if table is #tableau-recherche or
            // if no tags are selected
            if (settings.nTable.id !== 'tableau-recherche' || (Object.keys(selected_tags).length === 0)) {
                return true;
            }
            if (and_mode) {  // tag_1 AND tag_2 ...
                for (let tag in selected_tags) {
                    tag = tag.normalize("NFD").replace(/[\u0300-\u036f]/g, "");  // remove accentuation
                    let regex = new RegExp(`>${tag}<`);
                    if (!regex.test(searchData[2])) {
                        return false;
                    }
                }
                return true;
            } else {  // tag_1 OR tag_2 ...
                for (let tag in selected_tags) {
                    tag = tag.normalize("NFD").replace(/[\u0300-\u036f]/g, "")  // remove accentuation
                    let regex = new RegExp(`>${tag}<`);
                    if (regex.test(searchData[2])) {
                        return true;
                    }
                }
                return false;
            }
        }
    );


    /*
    * Add event listener to all tag buttons
    * Each times a tag button is clicked, it's added or removed from th selected_tags object
    * and table.draw() method is called (triggering a filter on table)
    */
    $('.tag-selection').on("click", function () {
        if (Object.hasOwn(selected_tags, $(this).text())) {
            delete selected_tags[$(this).text()]
        } else {
            selected_tags[$(this).text()] = true;
        }
        $(this).toggleClass(SELECTED_CLASS);
        table.draw();
    });

    /*
    * Add event listener to radio buttons
    * indicating the selection mode
    */
    $('#label-et-ou').on("click", function () {
        and_mode = !and_mode;
        if ($('#switch-et-ou').is(':checked')) {
            $('#switch-et-ou').prop('checked', true);
            $('#span-et').css('opacity', 1);
            $('#span-ou').css('opacity', 0.6);
        } else {
            $('#switch-et-ou').prop('checked', false);
            $('#span-ou').css('opacity', 1);
            $('#span-et').css('opacity', 0.6);
        }
        table.draw();
    });
}


/*
* Clear search input and trigger and table search
*/
function vide() {
    $("#recherche-nom").val("");
    $("#recherche-nom").trigger("keyup");
}
