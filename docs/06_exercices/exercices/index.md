---
author: Steeve PYTEL
title: 📋 Les Exercices
---

# Exercices


??? note "Programme de sciences numériques et technologie de seconde générale et technologique"

	<div class="centre">
	<iframe 
	src="/01_SNT/documents/BO.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>

??? note "1 - Fabrication de drapeaux"

    Exemples avec trois panneaux

    === "Cours"
        # Traiter des images avec Python

        !!! success ""
            À l'aide d'un programme en Python vous allez pouvoir :
            
            * créer des images pixel par pixel ;
            * intervenir sur chaque pixel et modifier sa couleur pour obtenir différents effets.
        
        !!! success ""
            ### Coordonnées des pixels dans une image ###
            Pour travailler avec une image il faut savoir repérer chaque pixel par ses coordonnées, comme un point dans un repère en mathématiques. Python utilise les coordonnées repérées comme indiqué sur l'image ci-dessous.
            <br>
            ![](../../assets/grille.png)
            <br>
            ### Quelques instructions indispensables ###
            #### Créer une nouvelle image ####
            `img=Image.new("RGB", (NombreColonnes, NombreLignes))` : crée une nouvelle image en mode **RGB**, chaque pixel est défini par son niveau de rouge (**R**ed), de vert (**G**reen) et de bleu(**B**lue) et dont la définition est données par son nombre de pixels dans les deux directions.
            #### Ouvrir une image déjà déposée dans Capytale ####
            `img=Image.open("nom_image")` : ouvre une image en spécifiant son nom suivi de son extension et la place dans une variable *img*.
            #### Connaître les composantes colorées d'un pixel ####
            `pixel = img.getpixel((Coord_X, Coord_Y))` : récupère les niveaux de rouge, de vert et de bleu comprise entre 0 et 255 et les place dans une variable *pixel*. `pixel[0]` contient le niveau de rouge, `pixel[1]` contient le niveau de vert et `pixel[2]` contient le niveau de bleu.
            #### Définir les composantes RVB d'un pixel ####
            `img.putpixel((Coord_X, Coord_Y), (R, V, B))` : définit la couleur du pixel repéré par ses coordonnées, à l'aide des niveaux de rouge, vert et bleu compris entre 0 et 255.


    === "Exemple"
        !!! exemple ""
            ## I. Création d'une image pixel par pixel ##
            ### Objectif ###
            Il s'agit de "fabriquer" une image en la définissant pixel par pixel.<br>
            On se propose de créer des images avec des zones géométriques simples : des drapeaux.
            ### Consignes ###
            Le premier programme est complété, vous devrez l'exécuter et répondre aux questions à sa suite.<br>
            Sur le même modèle, vous devrez ensuite construire :
            * le drapeau belge ;
            * le drapeau néerlandais ;
            * le drapeau danois.

        {{ IDE('./scripts/drapeau-france.py') }}

        !!! quote ""
            <div id="cible_1" class="center" style="display: flex;justify-content: center;align-content:center; flex-direction: column; margin:auto; min-height:5em; text-align:center">
            Le drapeau sera affiché ici
            </div>

    === "QCM"
        {{ multi_qcm(
        [
            "Quelle est la définition, en pixels, de l'image créée ?",
            ["500*900", "haute definition", "255"],
            [1]
            ],
            [
            "Quelle est la couleur du pixel défini à la ligne 12 ?",
            ["rouge", "bleu", "blanc"],
            [2]
            ],
            [
            "Quelle est la couleur du pixle défini à la ligne 14 ?",
            ["rouge", "bleu", "blanc"],
            [3]
            ],
            [
            "Quelle est la couleur du pixel défini à la ligne 16 ?",
            ["rouge", "bleu", "blanc"],
            [1]
            ],
            [
            "À quoi servent les deux boucles for des lignes 9 et 10 ?",
            ["Compter les pixels", "Parcourir toutes les lignes et colonnes", "Afficher l'image"],
            [2]
            ],
            [
            "Aux lignes 11 et 13, quel est l'utilité du test if i < nbColonnes / 3 pour la construction ?",
            ["Compter jusqu'a 3", "Couper en 3 colonnes", "Couper en 3 lignes"],
            [2]
            ],
        qcm_title = "Un Qcm sur le code",
        multi = False,
        DEBUG = False
        ) }}

        Maintenant d'autres drapeaux ! 
    === "Belge"
        !!! exemple ""
            ### Construire d'autres drapeaux
            En vous basant sur l"exemple de code, entrez les lignes manquantes dans la cellule ci-dessous pour construire les drapeaux en respectant les caractéristiques communiquées.

            Drapeau de la Belgique
            <div id="belge" class="center" style="display: flex;justify-content: center;align-content:center; flex-direction: column; margin:auto; min-height:5em; text-align:center">
            ![](../../assets/Belgique.png)
            </div>

        {{ IDE('./scripts/drapeau-belge.py') }}

        !!! quote ""
            <div id="cible_2" class="center" style="display: flex;justify-content: center;align-content:center; flex-direction: column; margin:auto; min-height:5em; text-align:center">
            Le drapeau sera affiché ici
            </div>
    === "Pays-Bas"
        !!! exemple ""
            ### Construire d'autres drapeaux
            En vous basant sur l"exemple de code, entrez les lignes manquantes dans la cellule ci-dessous pour construire les drapeaux en respectant les caractéristiques communiquées.

            Drapeau de Pays Bas
            <div id="PaysBas" class="center" style="display: flex;justify-content: center;align-content:center; flex-direction: column; margin:auto; min-height:5em; text-align:center">
            ![](../../assets/PaysBas.png)
            </div>

        {{ IDE('./scripts/drapeau-paysbas') }}

        !!! quote ""
            <div id="cible_3" class="center" style="display: flex;justify-content: center;align-content:center; flex-direction: column; margin:auto; min-height:5em; text-align:center">
            Le drapeau sera affiché ici
            </div>
    === "Danemark"
        !!! exemple ""
            ### Construire d'autres drapeaux
            En vous basant sur l"exemple de code, entrez les lignes manquantes dans la cellule ci-dessous pour construire les drapeaux en respectant les caractéristiques communiquées.

            Drapeau de Danemark
            <div id="Danemark" class="center" style="display: flex;justify-content: center;align-content:center; flex-direction: column; margin:auto; min-height:5em; text-align:center">
            ![](../../assets/Danemark.png)
            </div>

        {{ IDE('./scripts/drapeau-danemark.py') }}

        !!! quote ""
            <div id="cible_4" class="center" style="display: flex;justify-content: center;align-content:center; flex-direction: column; margin:auto; min-height:5em; text-align:center">
            Le drapeau sera affiché ici
            </div>