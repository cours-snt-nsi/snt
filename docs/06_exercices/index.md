---
author: Steeve PYTEL
title: 🏡 Exercices
---

Ce site propose des exercices d'apprentissage de l'algorithmique et de la programmation par le biais d'exercices variés. Le langage utilisé est [**Python**](https://www.python.org/).

Les exercices proposés ont été **écrits**, **testés**, **corrigés** et **améliorés** par des professeurs d'informatique du secondaire et du supérieur.

Aucune installation, aucune inscription ne sont nécessaires : tous les programmes sont exécutés sur votre machine, tablette ou téléphone.

!!! note "Débuter 🚀"

    - 🧭 [en cherchant un exercice](/06_exercices/recherche/) par titre, difficulté ou thématiques
    - 🚶 [en suivant un des parcours](/06_exercices/parcours/) proposés
    - 🐦 en choisissant [une épreuve pratique blanche](/06_exercices/exam/)
    - ✅ en consultant [la page d'aide](/06_exercices/aide/) qui précise le fonctionnement du site







