---
author: Steeve PYTEL
title: 📋 CAPYTALE
---

# Exercices

!!! abstract "Débuter avec les Outils en ligne"

    1. [Boucles](https://capytale2.ac-paris.fr/web/c/073b-742357){target='_blank'}
    2. [Si](https://capytale2.ac-paris.fr/web/c/6961-922493){target='_blank'}  
    3. [While](https://capytale2.ac-paris.fr/web/c/0285-1279188){target='_blank'}
    4. [Dessin](https://capytale2.ac-paris.fr/web/c/0285-1279188){target='_blank'}
    5. [Mélange](https://capytale2.ac-paris.fr/web/c/0285-1279188){target='_blank'}
    6. [Découverte Python](https://capytale2.ac-paris.fr/web/c/ca10-632362){target='_blank'}

!!! abstract "Activitées Pixel'Art"

    1. [Easy](https://capytale2.ac-paris.fr/web/c/dbee-644319){target='_blank'}
    2. [Drapeau](https://capytale2.ac-paris.fr/web/c/125f-1447786){target='_blank'}
    3. [Zig](https://capytale2.ac-paris.fr/web/c/4cc1-567778){target='_blank'}
    4. [Echec](https://capytale2.ac-paris.fr/web/c/ab28-802220){target='_blank'}
   

