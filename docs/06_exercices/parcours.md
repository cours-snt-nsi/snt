---
hide:
    - toc
---

# 🚶 Parcours

On propose ici différents parcours permettant d'explorer un algorithme, une thématique ou une structure de données...


??? abstract "Manipulation de chaines de caractères"

    Tout est dans le titre !

    - « {{ lien_exo("Dentiste", "dentiste" ) }} » : supprimer les voyelles d'une chaine de caractères
    - « {{ lien_exo("Mots qui se correspondent", "correspond_mot" ) }} » : comparer deux chaines de caractères
    - « {{ lien_exo("Renverser une chaine", "renverse_chaine" ) }} » : comme son nom l'indique !
    - « {{ lien_exo("Collage", "join" ) }} » : former une chaine à partir des éléments d'une liste... réécrire `" ".join(mots)` !
    - « {{ lien_exo("Découpe", "split" ) }} » : découper une chaine à chaque espace... réécrire `chaine.split(' ')` !
    - « {{ lien_exo("Code de César", "code_cesar" ) }} » : chiffrer une chaine de caractère à l'aide du code de César
    - « {{ lien_exo("Texte inclus", "est_inclus" ) }} » : recherche d'un motif dans une chaine de caractères
    - « {{ lien_exo("Texte brut", "brut" ) }} » : extraire le contenu textuel d'une source `HTML`

??? abstract "Maths : Seconde"

    - « {{ lien_exo("Fonctions affines", "fonctions_affines" ) }} » : plusieurs exercices autour des fonctions affines
    - « {{ lien_exo("Fonctions du 2nd degré", "second_degre_1" ) }} » : plusieurs exercices autour des fonctions polynômes du seconde degré
    - « {{ lien_exo("Géométrie vectorielle", "geometrie_vectorielle" ) }} » : plusieurs exercices de géométrie vectorielle
    - « {{ lien_exo("Résolution d'équation par balayage", "balayage" ) }} » : résolution approchée d'une équation
    - « {{ lien_exo("Moyenne simple", "moyenne" ) }} » : calculer une moyenne
    - « {{ lien_exo("Moyennes pondérée", "moy_ponderee" ) }} » : calculer une moyenne pondérée
    - « {{ lien_exo("Puissances d'un nombre", "seuil_puissances" ) }} » : déterminer la première puissance d'un nombre dépassant ou passant sous un seuil
    - « {{ lien_exo("Multiple", "multiple" ) }} » : déterminer si un entier est un multiple d'un autre
    - « {{ lien_exo("Diviseurs d'un entier", "diviseurs" ) }} » : déterminer les diviseurs d'un entier positif
    - « {{ lien_exo("Divisibilité par 3", "divisible_par_3" ) }} » : trois méthodes pour étudier la divisibilité par trois
    - « {{ lien_exo("Nombre Premier", "nombre_premier" ) }} » : déterminer si un (petit) nombre entier est premier

??? abstract "Structures conditionnelles"

    Si c'est vrai il faut faire ceci, sinon cela...

    - « {{ lien_exo("Autour des booléens", "autour_des_booleens" ) }} » : écrire des tests tels que « $15$ est-il dans la table de $3$ et de $5$ ? »
    - « {{ lien_exo("Années bissextiles", "bissextile" ) }} » : déterminer si une année est bissextile ou non
    - « {{ lien_exo("Opérateurs booléens", "operateurs_booleens" ) }} » : écrire les opérateurs booléens sans utiliser les opérateurs booléens !
    - « {{ lien_exo("Multiplier sans *", "multiplication" ) }} » : multiplier deux entiers relatifs sans utiliser le symbole `*`
    - « {{ lien_exo("Suite de Syracuse", "syracuse" ) }} » : la fameuse suite $3n+1$
    - « {{ lien_exo("Tous différents", "tous_differents" ) }} » : les éléments d'un tableau sont-ils tous distincts (solution en temps quadratique)
    - « {{ lien_exo("Noircir un texte", "anonymat_1") }} » et « {{ lien_exo("Caviarder un texte", "anonymat_2" ) }} » : effacer les caractères alphabétiques dans un texte