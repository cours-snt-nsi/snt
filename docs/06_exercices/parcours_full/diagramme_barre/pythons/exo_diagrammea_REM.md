Il est aussi possible de créer le diagramme en utilisant une liste en compréhension :

```python
def barres(categories, valeurs):
    taille = longueur_max(categories)
    return [allonge(categories[i], taille) + " : " + "#" * valeurs[i] for i in range(len(categories))]
```
