# --------- PYODIDE:code --------- #
def variations(a, b, c):
    ...


# --------- PYODIDE:corr --------- #
def variations(a, b, c):
    if a < 0:
        return "^"
    else:
        return "v"


# --------- PYODIDE:tests --------- #
assert variations(1, 0, 0) == "v"
assert variations(-1, -8, 3) == "^"

# --------- PYODIDE:secrets --------- #
a = 3
b = -2
c = 4
attendu = "v"
assert variations(a, b, c) == attendu, f"Erreur avec {a, b, c = }"
a = 2
b = 0
c = 5
attendu = "v"
assert variations(a, b, c) == attendu, f"Erreur avec {a, b, c = }"
a = -2
b = 6
c = 0
attendu = "^"
assert variations(a, b, c) == attendu, f"Erreur avec {a, b, c = }"
