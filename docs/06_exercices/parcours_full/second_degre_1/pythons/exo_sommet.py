# --------- PYODIDE:code --------- #
def sommet(a, b, c): 
    ...


# --------- PYODIDE:corr --------- #
def sommet(a, b, c):
    x = -b / (2 * a)
    y = a * x**2 + b * x + c
    return (x, y)


# --------- PYODIDE:tests --------- #
assert sommet(1, 0, 0) == (0.0, 0.0)
assert sommet(1, -8, 3) == (4.0, -13.0)

# --------- PYODIDE:secrets --------- #
a = 3
b = -2
c = 4
attendu = -b / (2 * a), a * (-b / (2 * a)) ** 2 + b * (-b / (2 * a)) + c
assert sommet(a, b, c) == attendu, f"Erreur avec {a, b, c = }"
a = 2
b = 0
c = 5
attendu = -b / (2 * a), a * (-b / (2 * a)) ** 2 + b * (-b / (2 * a)) + c
assert sommet(a, b, c) == attendu, f"Erreur avec {a, b, c = }"
a = -2
b = 6
c = 0
attendu = -b / (2 * a), a * (-b / (2 * a)) ** 2 + b * (-b / (2 * a)) + c
assert sommet(a, b, c) == attendu, f"Erreur avec {a, b, c = }"


