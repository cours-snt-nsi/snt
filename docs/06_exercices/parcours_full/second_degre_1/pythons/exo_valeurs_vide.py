# --------- PYODIDE:code --------- #
def f(x):
    return x**2


def valeurs(fonction, x_min, x_max): 
    ...


# --------- PYODIDE:corr --------- #
def valeurs(fonction, x_min, x_max):
    tableau = []
    for x in range(x_min, x_max + 1):
        tableau.append(fonction(x))
    return tableau


# --------- PYODIDE:tests --------- #
assert valeurs(f, 0, 1) == [0, 1]
assert valeurs(f, 0, 3) == [0, 1, 4, 9]


def g(x):
    return -2 * x**2 + 3 * x + 1


assert valeurs(g, -1, 1) == [-4, 1, 2]
assert valeurs(g, -1, 3) == [-4, 1, 2, -1, -8]

# --------- PYODIDE:secrets --------- #
a = -3
b = 4
c = -1


def h(x):
    return a * x**2 + b * x + c


x_min = -4
x_max = 5
attendu = [h(x) for x in range(x_min, x_max + 1)]
assert valeurs(h, x_min, x_max) == attendu, f"Erreur en prenant x ↦ {a}x**2+{b}x{c}"

a = -10
b = -3
c = 9


def h(x):
    return a * x**2 + b * x + c


x_min = -7
x_max = 12
attendu = [h(x) for x in range(x_min, x_max + 1)]
assert valeurs(h, x_min, x_max) == attendu, f"Erreur en prenant x ↦ {a}x**2{b}x+{c}"


