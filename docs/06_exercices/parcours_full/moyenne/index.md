---
author: Sébastien Hoarau
hide:
    - navigation
    - toc
title: Moyenne simple 
difficulty: 118
tags:
    - float
    - ep1
---

Écrire une fonction `moyenne` prenant en paramètre un tableau non vide d'entiers et qui renvoie la moyenne des valeurs du tableau.

???+ warning "Contraintes"

    Dans cet exercice, on n'utilisera pas la fonction prédéfinie `#!py sum` ni aucune autre fonction de quelque module que se soit calculant la moyenne.

{{ remarque('proximite_flottants')}}


???+ example "Exemples"

    ```pycon title=""
    >>> moyenne([10, 20, 30, 40, 60, 110])
    45.0
    >>> moyenne([1, 3])
    2.0
    >>> moyenne([44, 51, 12, 72, 65, 34])
    46.333333333333336
    ```

{{ IDE('exo', SANS="sum") }}
