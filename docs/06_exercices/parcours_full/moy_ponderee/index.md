---
author: Franck Chambon
hide:
    - navigation
    - toc
title: Moyenne pondérée
tags:
    - tuple
    - maths
    - ep1
difficulty: 230
maj: 01/03/2024
---


Les résultats aux évaluations d'un élève sont regroupés dans une liste non vide composée de couples `(note, coefficient)`. Dans ce couple :

- `note` est un nombre de type flottant (`float`) compris entre `#!py 0.0` et `#!py 20.0`
- `coefficient` est un nombre de type entier (`int`) strictement positif

Écrire une fonction `moyenne` qui renvoie la moyenne pondérée de cette liste donnée en paramètre.

{{ remarque('proximite_flottants')}}

???+ example "Exemple"
    Le calcul suivant illustre l'exemple :

    $$\frac{2×15,\!0 + 1×9,\!0 + 3×12,\!0}{2+1+3} = 12,\!5$$

    ```pycon
    >>> moyenne([(15.0, 2), (9.0, 1), (12.0, 3)])
    12.5
    ```

{{ IDE('exo') }}
