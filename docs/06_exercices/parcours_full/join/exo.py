

# --------- PYODIDE:code --------- #

def supprimheu(mots):
    ...

# --------- PYODIDE:corr --------- #

def supprimheu(mots):
    discours = ""
    for mot in mots:
        if mot != "heu":
            if discours != "":
                # on a déjà mis un mot
                discours += " "
            discours += mot
    return discours

# --------- PYODIDE:tests --------- #

assert (supprimheu(["je", "heu", "vais", "coder", "heu", "la", "fonction", "supprimheu"])
                  == "je vais coder la fonction supprimheu")


assert supprimheu(["c", "est", "facile"]) == "c est facile"

# --------- PYODIDE:secrets --------- #


# autres tests

assert supprimheu([]) == ""
assert supprimheu(["heu"]) == ""
assert supprimheu(["heu", "bonjour"]) == "bonjour"
assert supprimheu(["bien", "le", "bonjour"]) == "bien le bonjour"
assert supprimheu(["bien", "le", "bonjour", "heu"]) == "bien le bonjour"
