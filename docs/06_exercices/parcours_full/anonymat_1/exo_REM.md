La fonction `noircir` effectue les calculs sur tous les caractères de `texte`, on n'utilise pas leurs indices dans la chaîne. On peut donc directement parcourir les caractères de la chaîne avec la boucle `for caractere in texte`.

Il est aussi possible d'écrire cette fonction en utilisant une approche plus « fonctionnelle » :

```python
def noircir(texte, noir):
    return "".join([noir if c.isalpha() else c for c in texte])
```
