---
author: Nicolas Revéret
hide:
    - navigation
    - toc
difficulty: 50
title: Autour des booléens
tags:
    - booléen
---
# Autour des booléens

Un objet de type `bool` ne peut avoir que l'une des deux valeurs `#!py True` ou `#!py False` appelées **valeurs booléennes**. Ces valeurs permettent de donner une **valeur de vérité** à une proposition : elle est vraie ou fausse.

Par exemple, la proposition « *3 est strictement inférieur à 4* » est vraie. Traduite en Python, elle devient `#!py 3 < 4` et est évaluée à `#!py True`.

À l'inverse, la proposition « *le caractère "a" est présent dans le mot "chien"* » est fausse : l'expression `#!py "a" in "chien"` est évaluée par Python à `#!py False`.

Compléter le code ci-dessous en saisissant les propositions demandées **et non leur valeur de vérité**.

Afin de corriger les propositions, le résultat de l'évaluation par Python de chaque proposition est affecté à une variable (`prop_1`, `prop_2`...). Ce sont les valeurs de ces variables qui sont vérifiées dans la correction automatique. 

!!! tip "Astuce"

    Il est possible d'évaluer les différentes propositions dans la console avant de les saisir dans l'éditeur.

    {{ terminal() }}

{{ IDE('exo') }}