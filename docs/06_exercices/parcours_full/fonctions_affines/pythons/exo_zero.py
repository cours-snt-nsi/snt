

# --------- PYODIDE:code --------- #

def antecedent_zero(a, b):
    ...

# --------- PYODIDE:corr --------- #

def antecedent_zero(a, b):
    return -b / a

# --------- PYODIDE:tests --------- #

assert abs(antecedent_zero(4, -8) - 2) < 1e-6
assert abs(antecedent_zero(-2, 15) - 7.5) < 1e-6

# --------- PYODIDE:secrets --------- #


# tests secrets
from random import randrange

for test in range(10):
    a = randrange(-20, 20)
    b = randrange(-50, 50)
    if a == 0:
        continue
    attendu = -b / a
    assert (
        abs(antecedent_zero(a, b) - attendu) < 1e-6
    ), f"Erreur en calculant antecedent_zero{a, b}"