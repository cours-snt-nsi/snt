

# --------- PYODIDE:code --------- #

def variations(a, b):
    ...

# --------- PYODIDE:corr --------- #

def variations(a, b):
    if a > 0:
        return "strictement croissante"
    elif a == 0:
        return "constante"
    else:
        return "strictement décroissante"

# --------- PYODIDE:tests --------- #

assert variations(4, -8) == "strictement croissante"
assert variations(-2, 15) == "strictement décroissante"

# --------- PYODIDE:secrets --------- #
from random import randrange

# tests secrets
a = 0
b = randrange(-50, 50)
attendu = "constante"
assert variations(a, b) == attendu, f"Erreur en calculant variations{a, b}"

from random import randrange

for test in range(10):
    a = randrange(-20, 20)
    b = randrange(-50, 50)
    attendu = "strictement croissante" if a > 0 else "constante" if a == 0 else "strictement décroissante"
    assert variations(a, b) == attendu, f"Erreur en calculant variations{a, b}"