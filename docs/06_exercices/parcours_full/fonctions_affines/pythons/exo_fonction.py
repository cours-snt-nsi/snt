

# --------- PYODIDE:env --------- #

def g(x):
    pass

def f(x):
    return 5 * x - 9


# --------- PYODIDE:code --------- #

def f(x):
    return 5 * x - 9


# compléter ci-dessous avec votre fonction
...

# --------- PYODIDE:corr --------- #

def g(x):
    return -3 * x + 7

# --------- PYODIDE:tests --------- #

# la fonction f n'a pas été modifiée
assert f(0) == -9
assert f(10) == 41
# définition correcte de la fonction g
assert g(0) == 7
assert g(10) == -23

# --------- PYODIDE:secrets --------- #


# tests secrets
def _g(x):
    return -3 * x + 7


for x in range(-10, 11):
    attendu = _g(x)
    assert g(x) == attendu, f"Erreur en calculant g({x})"