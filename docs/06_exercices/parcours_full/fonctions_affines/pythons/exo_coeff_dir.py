

# --------- PYODIDE:env --------- #

def coefficient_directeur(x_A, y_A, x_B, y_B):
    pass

# --------- PYODIDE:code --------- #

def coefficient_directeur(x_A, y_A, x_B, y_B):
    ...

# --------- PYODIDE:corr --------- #

def coefficient_directeur(x_A, y_A, x_B, y_B):
    delta_y = y_B - y_A
    delta_x = x_B - x_A
    return delta_y / delta_x

# --------- PYODIDE:tests --------- #

assert abs(coefficient_directeur(1, 4, 9, 10) - 0.75) < 1e-6
assert abs(coefficient_directeur(3, 15, -2, 15) - 0) < 1e-6

# --------- PYODIDE:secrets --------- #


# tests secrets
from random import randrange

for test in range(10):
    x_A = randrange(-50, 50)
    y_A = randrange(-50, 50)
    x_B = randrange(-50, 50)
    y_B = randrange(-50, 50)
    if x_A == x_B:
        continue
    attendu = (y_B - y_A) / (x_B - x_A)
    assert (
        abs(coefficient_directeur(x_A, y_A, x_B, y_B) - attendu) < 1e-6
    ), f"Erreur en calculant coefficient_directeur{x_A, y_A, x_B, y_B}"