

# --------- PYODIDE:code --------- #

def somme(a, b):
    return ...  # ← compléter ici

# --------- PYODIDE:corr --------- #

def somme(a, b):
    return a + b

# --------- PYODIDE:tests --------- #

assert somme(10, 32) == 42
assert somme(100, 7) == 107

# --------- PYODIDE:secrets --------- #

# tests

assert somme(10, 32) == 42


# autres tests

assert somme(-10, 10) == 0

assert somme(1700, 89) == 1789
