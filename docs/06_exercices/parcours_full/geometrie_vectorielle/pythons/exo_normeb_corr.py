from math import sqrt


def norme(x_u, y_u):
    return sqrt(x_u**2 + y_u**2)


