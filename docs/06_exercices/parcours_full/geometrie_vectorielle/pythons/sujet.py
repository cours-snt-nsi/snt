# --- exo, milieu --- #
def milieu(x_A, y_A, x_B, y_B):
    x_M = ...
    y_M = ...
    return (..., ...)


# --- vide, milieu --- #
def milieu(x_A, y_A, x_B, y_B):
    ...


# --- corr, milieu --- #
def milieu(x_A, y_A, x_B, y_B):
    x_M = (x_A + x_B) / 2
    y_M = (y_A + y_B) / 2
    return (x_M, y_M)


# --- tests, milieu --- #
assert milieu(0, 2, 3, 6) == (1.5, 4.0)
assert milieu(10, 4, 5, -8) == (7.5, -2.0)
# --- secrets, milieu --- #
from random import randrange

for _ in range(10):
    x_A = randrange(-200, 200) / 2
    x_B = randrange(-200, 200) / 2
    y_A = randrange(-200, 200) / 2
    y_B = randrange(-200, 200) / 2
    x_attendu = (x_A + x_B) / 2
    y_attendu = (y_A + y_B) / 2
    assert milieu(x_A, y_A, x_B, y_B) == (
        x_attendu,
        y_attendu,
    ), f"Erreur avec A {x_A, y_A} et B {x_B, y_B}"


# --- exo, vecteur --- #
def vecteur(x_A, y_A, x_B, y_B):
    x_AB = ...
    ...
    return (..., ...)


# --- vide, vecteur --- #
def vecteur(x_A, y_A, x_B, y_B):
    ...


# --- corr, vecteur --- #
def vecteur(x_A, y_A, x_B, y_B):
    x_AB = x_B - x_A
    y_AB = y_B - y_A
    return (x_AB, y_AB)


# --- tests, vecteur --- #
assert vecteur(0, 2, 3, 6) == (3, 4)
assert vecteur(10, 4, 5, -8) == (-5, -12)
# --- secrets, vecteur --- #
from random import randrange

for _ in range(10):
    x_A = randrange(-200, 200) / 2
    x_B = randrange(-200, 200) / 2
    y_A = randrange(-200, 200) / 2
    y_B = randrange(-200, 200) / 2
    x_attendu = x_B - x_A
    y_attendu = y_B - y_A
    assert vecteur(x_A, y_A, x_B, y_B) == (
        x_attendu,
        y_attendu,
    ), f"Erreur avec A {x_A, y_A} et B {x_B, y_B}"


# --- vide, norme --- #
from math import sqrt


def norme(x_u, y_u):
    ...


# --- corr, norme --- #
from math import sqrt


def norme(x_u, y_u):
    return sqrt(x_u**2 + y_u**2)


# --- tests, norme --- #
assert norme(3, 4) == 5.0
assert norme(-5, -12) == 13.0
# --- secrets, norme --- #
from random import randrange

for _ in range(10):
    x_U = randrange(-200, 200) / 2
    y_U = randrange(-200, 200) / 2
    attendu = sqrt(x_U**2 + y_U**2)
    assert abs(norme(x_U, y_U) - attendu) < 10**-6, f"Erreur avec vecteur(u) {x_U, y_U}"
# --- vide, distance --- #
from math import sqrt


def distance(x_A, y_A, x_B, y_B):
    ...


# --- corr, distance --- #
from math import sqrt


def distance(x_A, y_A, x_B, y_B):
    return sqrt((x_B - x_A) ** 2 + (y_B - y_A) ** 2)


# --- tests, distance --- #
assert distance(0, 2, 3, 6) == 5.0
assert distance(10, 4, 5, -8) == 13.0
# --- secrets, distance --- #
from random import randrange

for _ in range(10):
    x_A = randrange(-200, 200) / 2
    x_B = randrange(-200, 200) / 2
    y_A = randrange(-200, 200) / 2
    y_B = randrange(-200, 200) / 2
    attendu = sqrt((x_B - x_A) ** 2 + (y_B - y_A) ** 2)
    assert (
        abs(distance(x_A, y_A, x_B, y_B) - attendu) < 10**-6
    ), f"Erreur avec A {x_A, y_A} et B {x_B, y_B}"


# --- exo, colineaire --- #
def colineaires(x_u, y_u, x_v, y_v):
    if ... == ...:
        return ...
    else:
        return ...


# --- vide, colineaire --- #
def colineaires(x_u, y_u, x_v, y_v):
    ...


# --- corr, colineaire --- #
def colineaires(x_u, y_u, x_v, y_v):
    if x_u * y_v - y_u * x_v == 0:
        return True
    else:
        return False


# --- rem, colineaire --- #
""" # skip
Dans le cas où l'expression `#!py x_u * y_v - y_u * x_v == 0` est évaluée à `#!py True` on renvoie `#!py True`.

Si elle est évaluée à  `#!py True` on renvoie `#!py False`.

On peut aussi bien renvoyer le résultat du test :

```python
def colineaires(x_u, y_u, x_v, y_v):
    return x_u * y_v - y_u * x_v == 0
```
"""  # skip
# --- tests, colineaire --- #
assert colineaires(3, 8, -6, -16) is True
assert colineaires(3, 8, 6, 17) is False
# --- secrets, colineaire --- #
from random import randrange

for _ in range(10):
    x_u = randrange(-200, 200) / 2
    x_v = randrange(-200, 200) / 2
    y_u = randrange(-200, 200) / 2
    y_v = randrange(-200, 200) / 2
    attendu = x_u * y_v - y_u * x_v == 0
    assert (
        colineaires(x_u, y_u, x_v, y_v) is attendu
    ), f"Erreur avec vecteur(u) {x_u, y_u} et vecteur(v) {x_v, y_v}"


# --- hdr, alignes, alignes_bis --- #
def colineaires(x_u, y_u, x_v, y_v):
    return x_u * y_v - y_u * x_v == 0


def vecteur(x_A, y_A, x_B, y_B):
    x_AB = x_B - x_A
    y_AB = y_B - y_A
    return (x_AB, y_AB)


# --- hdr, distance --- #
from math import sqrt

def norme(x_u, y_u):
    return sqrt(x_u**2 + y_u**2)


def vecteur(x_A, y_A, x_B, y_B):
    x_AB = x_B - x_A
    y_AB = y_B - y_A
    return (x_AB, y_AB)


# --- vide, alignes --- #
def alignes(x_A, y_A, x_B, y_B, x_C, y_C):
    ...


# --- exo, alignes --- #
def alignes(x_A, y_A, x_B, y_B, x_C, y_C):
    x_AB = ...
    ...
    x_AC = ...
    ...
    if ...:
        return ...
    else:
        return ...


# --- vide, alignes_bis --- #
def alignes(x_A, y_A, x_B, y_B, x_C, y_C):
    x_AB, y_AB = vecteur(..., ..., ..., ...)
    x_AC, y_AC = vecteur(..., ..., ..., ...)
    return ...


# --- corr, alignes --- #
def alignes(x_A, y_A, x_B, y_B, x_C, y_C):
    x_AB = x_B - x_A
    y_AB = y_B - y_A
    x_AC = x_C - x_A
    y_AC = y_C - y_A
    if x_AB * y_AC - y_AB * x_AC == 0:
        return True
    else:
        return False


# --- corr, alignes_bis --- #
def alignes(x_A, y_A, x_B, y_B, x_C, y_C):
    x_AB, y_AB = vecteur(x_A, y_A, x_B, y_B)
    x_AC, y_AC = vecteur(x_A, y_A, x_C, y_C)
    return colineaires(x_AB, y_AB, x_AC, y_AC)


# --- tests, alignes, aligne_bis --- #
assert alignes(-2, 2, 1, 1, 7, -1) is True
assert alignes(-2, 2, 1, 1, 7, -2) is False
# --- secrets, alignes, alignes_bis --- #
from random import randrange

for _ in range(10):
    x_A = randrange(-200, 200) / 2
    x_B = randrange(-200, 200) / 2
    x_C = randrange(-200, 200) / 2
    y_A = randrange(-200, 200) / 2
    y_B = randrange(-200, 200) / 2
    y_C = randrange(-200, 200) / 2
    attendu = (x_B - x_A) * (y_C - y_A) - (y_B - y_A) * (x_C - x_A) == 0
    assert (
        alignes(x_A, y_A, x_B, y_B, x_C, y_C) is attendu
    ), f"Erreur avec A {x_A, y_A}, B {x_B, y_B} et  C {x_C, y_C}"
# --- rem, distance --- #
""" # skip
Il est aussi possible d'utiliser les fonctions précédentes :

```python
def distance(x_A, y_A, x_B, y_B):
    x_AB, y_AB = vecteur(x_A, y_A, x_B, y_B)
    return norme(x_AB, y_AB)
```
"""  # skip
# --- rem, alignes --- #
""" # skip
Il est aussi de simplifier la fonction en renvoyant directement le résultat de la comparaison :

```python
def alignes(x_A, y_A, x_B, y_B, x_C, y_C):
    x_AB = x_B - x_A
    y_AB = y_B - y_A
    x_AC = x_C - x_A
    y_AC = y_C - y_A
    return x_AB * y_AC - y_AB * x_AC == 0
```
"""  # skip
