# --- HDR --- #
from math import sqrt

def norme(x_u, y_u):
    return sqrt(x_u**2 + y_u**2)


def vecteur(x_A, y_A, x_B, y_B):
    x_AB = x_B - x_A
    y_AB = y_B - y_A
    return (x_AB, y_AB)


# --- HDR --- #
# tests
assert distance(0, 2, 3, 6) == 5.0
assert distance(10, 4, 5, -8) == 13.0
