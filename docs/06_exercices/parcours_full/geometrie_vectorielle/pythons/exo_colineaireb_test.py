# tests
assert colineaires(3, 8, -6, -16) is True
assert colineaires(3, 8, 6, 17) is False
# tests secrets
from random import randrange

for _ in range(10):
    x_u = randrange(-200, 200) / 2
    x_v = randrange(-200, 200) / 2
    y_u = randrange(-200, 200) / 2
    y_v = randrange(-200, 200) / 2
    attendu = x_u * y_v - y_u * x_v == 0
    assert (
        colineaires(x_u, y_u, x_v, y_v) is attendu
    ), f"Erreur avec vecteur(u) {x_u, y_u} et vecteur(v) {x_v, y_v}"


