# tests
assert norme(3, 4) == 5.0
assert norme(-5, -12) == 13.0
# tests secrets
from random import randrange

for _ in range(10):
    x_U = randrange(-200, 200) / 2
    y_U = randrange(-200, 200) / 2
    attendu = sqrt(x_U**2 + y_U**2)
    assert abs(norme(x_U, y_U) - attendu) < 10**-6, f"Erreur avec vecteur(u) {x_U, y_U}"
