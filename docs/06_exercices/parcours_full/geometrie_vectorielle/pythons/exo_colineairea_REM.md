Dans le cas où l'expression `#!py x_u * y_v - y_u * x_v == 0` est évaluée à `#!py True` on renvoie `#!py True`.

Si elle est évaluée à  `#!py False` on renvoie `#!py False`.

On peut aussi bien renvoyer le résultat du test :

```python
def colineaires(x_u, y_u, x_v, y_v):
    return x_u * y_v - y_u * x_v == 0
```
