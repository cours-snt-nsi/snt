# tests secrets
from random import randrange

for _ in range(10):
    x_A = randrange(-200, 200) / 2
    x_B = randrange(-200, 200) / 2
    x_C = randrange(-200, 200) / 2
    y_A = randrange(-200, 200) / 2
    y_B = randrange(-200, 200) / 2
    y_C = randrange(-200, 200) / 2
    attendu = (x_B - x_A) * (y_C - y_A) - (y_B - y_A) * (x_C - x_A) == 0
    assert (
        alignes(x_A, y_A, x_B, y_B, x_C, y_C) is attendu
    ), f"Erreur avec A {x_A, y_A}, B {x_B, y_B} et  C {x_C, y_C}"
