# --- PYODIDE:code --- #
def puissance_superieure(a, b):
    ...


# --- PYODIDE:corr --- #
def puissance_superieure(a, b):
    puissance = 1
    n = 0
    while puissance < b:
        puissance = puissance * a
        n = n + 1
    return n


# --------- PYODIDE:tests --------- #
assert puissance_superieure(2, 1) == 0    # 2^0 = 1
assert puissance_superieure(2, 8) == 3    # 2^3 = 8
assert puissance_superieure(5, 130) == 4  # on a 5^3 = 125 et 5^4 = 625
# --------- PYODIDE:secrets --------- #
from math import log
from random import randrange

for _ in range(10):
    x = randrange(2, 10)
    seuil = randrange(1, 10**6)
    attendu = int(log(seuil, x)) + 1
    assert puissance_superieure(x, seuil) == attendu, f"Erreur avec {x = } et {seuil = }"

