# --------- PYODIDE:env --------- #
racine_de_2 = 2**0.5


def largeur_valide(a, b, h):
    largeur = abs(b - a)
    return abs(largeur - h) < 1e-6


# --------- PYODIDE:code --------- #
def balayage(a, h):
    ...


# --------- PYODIDE:corr --------- #
def balayage(a, h):
    x = a
    while (x + h) ** 2 <= 2:
        x = x + h
    return (x, x + h)


# --------- PYODIDE:tests --------- #
x, x_plus_h = balayage(1, 0.1)
# la solution est-elle comprise dans l'intervalle ?
assert x <= racine_de_2 < x_plus_h, "La solution cherchée n'est pas dans l'encadrement"
# l'encadrement est-il bien de largeur 0.1 ?
assert largeur_valide(x, x_plus_h, 0.1), "L'encadrement n'a pas la largeur attendue"

x, x_plus_h = balayage(1.4, 0.01)
assert x <= racine_de_2 < x_plus_h, "La solution cherchée n'est pas dans l'encadrement"
assert largeur_valide(x, x_plus_h, 0.01), "L'encadrement n'a pas la largeur attendue"

# --------- PYODIDE:secrets --------- #
a, h = 1.41, 0.001
x, x_plus_h = balayage(a, h)
assert (
    x <= racine_de_2 < x_plus_h
), f"La racine carré de 2 n'est pas dans l'encadrement avec {(a, h) = }"
assert largeur_valide(x, x_plus_h, 0.001), "L'encadrement n'a pas la largeur attendue"

for n in range(4, 6):
    a = 1
    h = 10**-n
    x, x_plus_h = balayage(a, h)
    assert (
        x <= racine_de_2 < x_plus_h
    ), f"La solution cherchée n'est pas dans l'encadrement avec {(a, h) = }"
    assert largeur_valide(
        x, x_plus_h, h
    ), f"L'encadrement n'a pas la largeur attendue avec {(a, h) = }"
    a = 1.3
    x, x_plus_h = balayage(a, h)
    assert (
        x <= racine_de_2 < x_plus_h
    ), f"La solution cherchée n'est pas dans l'encadrement avec {(a, h) = }"
    assert largeur_valide(
        x, x_plus_h, h
    ), f"L'encadrement n'a pas la largeur attendue avec {(a, h) = }"
