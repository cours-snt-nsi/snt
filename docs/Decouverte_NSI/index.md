---
author: N. Revéret
title: "🎓 Decouverte NSI"
---
# 🛫 Découverte de la spécialité NSI

On propose ici quelques travaux pratiques permettant à des élèves de 2nde de découvrir la spécialité NSI.

## Introduction au langage Python

- Algorea adventure : [Algorea adventure](https://parcours.algorea.org/contents/4707-4702-100575556387408660/)
    - Chapitre 1, affichage et ordre des instructions : [chapitre 1](https://parcours.algorea.org/contents/4707-4702-100575556387408660-1788359139685642917/)
    - Chapitre 2, les boucles bornées : [chapitre 2](https://parcours.algorea.org/contents/4707-4702-100575556387408660-1410985367628944162/)


## Python 1/4

- Tp-cours sur les entrées-sorties clavier, les variables et le type des données : [pdf](documents/python_affectation_entrees_sorties.pdf) ou [odt](documents/python_affectation_entrees_sorties.odt)


??? note "Python 1/4 "

    <div style="text-align: center;">
        <iframe 
            src="documents/python_affectation_entrees_sorties.pdf"
            width="1000" height="1000" 
            frameborder="0" 
            allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
        </iframe>
    </div>

## Python 2/4

- Tp-cours sur la structure conditionnelle : [pdf](documents/python_structure_conditionnelle.pdf) ou [odt](documents/python_structure_conditionnelle.odt)
??? note "Python 2/4 "

    <div style="text-align: center;">
        <iframe 
            src="documents/python_structure_conditionnelle.pdf"
            width="1000" height="1000" 
            frameborder="0" 
            allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
        </iframe>
    </div>


## Python 3/4

- Tp-cours sur la structure itérative : [pdf](documents/python_structure_itérative.pdf) ou [odt](documents/python_structure_itérative.odt)

??? note "Python 3/4 "

    <div style="text-align: center;">
        <iframe 
            src="documents/python_structure_itérative.pdf"
            width="1000" height="1000" 
            frameborder="0" 
            allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
        </iframe>
    </div>





## Python 4/4
Pour continuer :[rebondir une balle](01/tp_1.md) !
