#############
# Import(s) #
#############
from p5 import *

######################
# Variables globales #
######################
BLANC = (255, 255, 255)
NOIR = (0, 0, 0)
ROUGE = (255, 0, 0)
VERT = (0, 255, 0)
BLEU = (0, 0, 255)
diametre = 25
largeur = 500
hauteur = 500
mode = "D"
couleur = NOIR


####################################
# Fonctions exécutées au lancement #
####################################
def setup():
    """Initialisation de la figure"""
    size(largeur, hauteur)
    noLoop()  # supprime le rafraichissement de la figure


def draw():
    """Dessin UNE SEULE FOIS"""
    background(*BLANC)
    fill(50)
    text("HAUT / BAS: modifier le diamètre/côté", (5, 400))
    text("N : couleur Noire", (5, 420))
    text("B : couleur Bleue", (5, 440))
    text("D : mode Disque", (5, 460))
    text("C : mode Carré", (5, 480))


def mouse_pressed():
    """Actions effectuées lors d'un clic de souris"""
    fill(*couleur)
    print(mouse_x, mouse_y)
    if mode == "D":
        circle(mouse_x, mouse_y, diametre)
    elif mode == "C":
        rect((mouse_x, mouse_y), diametre, diametre)


def key_pressed():
    global diametre
    global mode
    global couleur
    if key == "UP":
        diametre = diametre + 20
    elif key == "DOWN":
        diametre = max(diametre - 20, 1)
    elif key == "C":
        mode = "C"
    elif key == "D":
        mode = "D"
    elif key == "R":
        couleur = ROUGE
    elif key == "N":
        couleur = NOIR

##########################
# Lancement du programme #
##########################
run(frame_rate=60)
