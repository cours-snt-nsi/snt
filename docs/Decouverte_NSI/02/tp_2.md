---
title: 👩‍🎨 Logiciel de dessin
author: N. Revéret
---

# 👩‍🎨 Logiciel de dessin

Nous avons découvert dans le [premier TP](../01/tp_1.md) les bases de `Python` et da la bibliothèque `p5`.

Utilisons-les afin de dessiner.

## Un premier *clic*

??? question-nico "Le bon fichier"

    1. Télécharger le fichier [tp_2.py](python/tp_2.py){:download="tp_2.py"} ;
   
    2. L'ouvrir avec Thonny ;
   
    3. L'exécuter en cliquant sur ▶️.


??? question-nico "Rien ne se passe ?"

    Lisez bien le code exécuté... Vous devriez comprendre quoi faire et où regarder !
    
??? question-nico "1 *clic* = 1 `circle`"

    Lors du clic de la souris, actuellement on affiche les coordonnées du clic en faisant `print(mouse_x, mouse_y)`.
    
    Pourquoi ne pas dessiner un cercle à l'emplacement du clic ? Il suffit de modifier le contenu de la fonction `moused_pressed`.
    
    On rappelle que les instructions suivantes dessinent un cercle rouge de centre `#!py (100, 50)` et de diamètre égal au contenu de la variable `#!py diametre` (déjà définie) :
    
    ```python
    fill(*ROUGE)
    circle(100, 50, diametre)
    ```

![Petits disques](images/petit.PNG){.center width=30%}

## Dessiner en grand !

Le cercle dessiné n'est pas très grand. Nous allons améliorer cela.
    
??? question-nico "(en grand) ⨉ 1"

    Modifier la valeur initiale de la variable `diametre` afin de dessiner en plus grand.
    
Il serait tout de même plus pratique de pouvoir modifier la variable `diametre` en *live* pendant le dessin.
Pour ce faire nous allons intercepter des évènements provenant du clavier !
    
Les évènements provenant du clavier sont gérés par la fonction `key_pressed`.

Dans cette fonction, on peut utiliser la variable `key` (définie par défaut) afin de savoir quelle touche a été pressée.

??? question-nico "(en grand) ⨉ N"

    Compléter le code avec les instructions ci-dessous :    
    
    ```python
    def key_pressed():
        global diametre
        global mode
        global couleur
        if key == "UP":  # (1)
            diametre = diametre + 1
    ```

    1. Recopier cette ligne et la suivante !
    
Le disque grandit petit à petit... Mais que c'est lent !

??? question-nico "Plus vite !!"

    Modifier la fonction `key_pressed` afin que le diamètre augmente plus rapidement.

Plus grand c'est bien. Mais parfois on préfère être précis et dessiner en petit.

??? question-nico "(en petit) ⨉ N"

    Rajouter un test à la fonction `key_pressed` afin de réduire le diamètre
    
    ??? tip "Aide"
    
        La fonction pourra ressembler à cela :
        
        ```python
        def key_pressed():
            global diametre
            global mode
            global couleur
            if key == "UP":
                diametre = diametre + 1
            elif key == "...":
                diametre = max(1, diametre ...)  # (1)
        ```

        1. On utilise `#!py max` pour bloquer la valeur à `#!py 1` pixel.
        
## On termine en beauté !

Vous avez réussi tout ce qui précède ? Voici quelques propositions d'améliorations :

1. Modifier la couleur en utilisant les touches `#!py "N"` (pour `#!py NOIR`) et `#!py "R"` (pour `#!py ROUGE`) ;

2. Dessiner des carrés ou des disques en utilisant les touches `#!py "C"` ou `#!py "D"`.

??? tip "Aide (1)"

    La couleur utilisée lors du dessin est déjà créée. C'est elle qu'il faut modifier lors des frappes au clavier.

??? tip "Aide (2)"

    C'est plus compliqué. Il faut :
    
    * intercepter les frappes des touches `#!py "C"` ou `#!py "D"` et modifier la variable `#!py mode` en conséquence ;
    
    * tester, lors de chaque clic de souris, le mode et faire le dessin en conséquence.

??? tip "Aide (3)"

    Pour dessiner un carré, on peut faire : `#!py rect(mouse_x, mouse_y, diametre, diametre)`.
    
![Version finale](images/complet.png){.center width=30%}
