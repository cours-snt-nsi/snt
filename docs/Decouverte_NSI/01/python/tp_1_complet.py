#############
# Import(s) # (1)
#############
from p5 import *

######################
# Variables globales # (2)
######################
BLANC = (255, 255, 255)
NOIR = (0, 0, 0)
ROUGE = (255, 0, 0)
x = 100
y = 25
diametre = 50
vy = 1
largeur = 200
hauteur = 500

####################################
# Fonctions exécutées au lancement # (3)
####################################
def setup():
    """Initialisation de la figure"""
    size(largeur, hauteur)


def draw():
    """Dessin à chaque étape"""
    global x  # permet de modifier x (atuce)
    global y  # permet de modifier y (atuce)
    global vy  # permet de modifier vy (atuce)

    # Le fond
    background(*BLANC)


    # y = y + vy
    # Dessin d'un disque
    fill(*NOIR)  # couleur intérieure
    stroke(*ROUGE)  # couleur du bord
    strokeWeight(2)  # largeur de la bordure
    circle(x, y, diametre)


def mouse_pressed():
    """Actions effectuées lors d'un clic de souris"""
    pass


##########################
# Lancement du programme # (4)
##########################
run(frame_rate=60)
