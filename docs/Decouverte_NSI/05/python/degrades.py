#############
# Import(s) #
#############
from p5 import *
from random import randrange

######################
# Variables globales #
######################

# La fenêtre
LARGEUR = 400
HAUTEUR = 700
# Les couleurs
BLANC = (255, 255, 255)
NOIR = (0, 0, 0)
GRIS = (50, 50, 50)

PAS = 3


####################################
# Fonctions exécutées au lancement #
####################################
def setup():
    """Initialisation de la figure"""
    size(LARGEUR, HAUTEUR)
    # stroke_weight(0)
    # stroke(*NOIR)
    noLoop()


def draw():
    """Dessin à chaque étape"""
    # Le fond
    background(*BLANC)

    nb_rectangles = LARGEUR // PAS

    rouge = 255
    vert = 255
    bleu = 0
    for x in range(0, LARGEUR, PAS):
        rouge = rouge + 255 / nb_rectangles
        vert = vert - 255 / nb_rectangles
        # bleu = bleu - 255 / nb_rectangles
        fill(int(rouge), int(vert), int(bleu))
        stroke(int(rouge), int(vert), int(bleu))
        rect(x, 0, x + PAS, HAUTEUR)

    save_frame("degrades.png")


run(frame_rate=60)
