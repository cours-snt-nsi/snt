#############
# Import(s) #
#############
from p5 import *
from random import randrange

######################
# Variables globales #
######################

# La fenêtre
LARGEUR = 1000
HAUTEUR = 600
# Les couleurs
BLANC = (255, 255, 255)
NOIR = (0, 0, 0)
GRIS = (50, 50, 50)

x_centre = LARGEUR // 2
y_centre = HAUTEUR // 2
rayon_max = LARGEUR // 50
rayon_disque = HAUTEUR // 4
nb_disques = 1000
PAS = 10


####################################
# Fonctions exécutées au lancement #
####################################
def setup():
    """Initialisation de la figure"""
    size(LARGEUR, HAUTEUR)
    stroke_weight(1)
    stroke(*NOIR)
    noLoop()


def draw():
    """Dessin à chaque étape"""
    # Le fond
    background(*BLANC)

    for x in range(PAS, LARGEUR // 2, PAS):
        rayon = randrange(0, x//10)
        couleur = randrange(10, 200)
        fill(couleur, couleur, couleur)
        circle(x, y_centre, rayon)
        circle(LARGEUR - x, y_centre, rayon)
        

    for _ in range(nb_disques):
        x = randrange(- rayon_disque,rayon_disque)
        pos_x = int((rayon_disque**2 - x**2) ** 0.5)
        if pos_x != 0:
            y = randrange(-pos_x, pos_x)
            rayon = randrange(0, rayon_max)
            couleur = randrange(10, 200)
            fill(couleur, couleur, couleur)
            circle(x_centre + x, y_centre + y, rayon)

    save_frame("toto.png")


run(frame_rate=60)
