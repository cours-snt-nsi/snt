#############
# Import(s) #
#############
from p5 import *
from random import randrange

######################
# Variables globales #
######################

# La fenêtre
LARGEUR = 1000
HAUTEUR = 600
# Les couleurs
BLANC = (255, 255, 255)
NOIR = (0, 0, 0)
GRIS = (50, 50, 50)

x_centre = LARGEUR // 2
y_centre = HAUTEUR // 2
rayon_max = LARGEUR // 50
rayon_disque = HAUTEUR // 4
NB_TRIANGLES = 20
PAS = LARGEUR // NB_TRIANGLES
RACINE_3_SUR_2 = 1.15 * (3**0.5) / 2


####################################
# Fonctions exécutées au lancement #
####################################
def setup():
    """Initialisation de la figure"""
    size(LARGEUR, HAUTEUR)
    stroke_weight(1)
    stroke(*BLANC)
    noLoop()


def draw():
    """Dessin à chaque étape"""
    # Le fond
    background(*BLANC)

    for x in range(0, LARGEUR + PAS, PAS):
        for y in range(int(RACINE_3_SUR_2) * PAS, HAUTEUR + PAS, PAS):
            fill(randrange(0, 256), randrange(0, 256), randrange(0, 256))
            triangle(x, y, x + PAS, y, x + PAS // 2, y - RACINE_3_SUR_2 * PAS)
            fill(randrange(0, 256), randrange(0, 256), randrange(0, 256))
            triangle(
                x - PAS // 2,
                y - RACINE_3_SUR_2 * PAS,
                x + PAS // 2,
                y - RACINE_3_SUR_2 * PAS,
                x,
                y,
            )

    save_frame("triangles.png")


run(frame_rate=60)
