---
title: 💻 Fond d'écran
author: N. Revéret
---

# 💻 Fond d'écran

Ce TP vous propose de créer vos propres fond d'écran (d'ordinateur, de téléphone...).

![Dégradé bleu](images/degrades.png){.center width=50% style="border:solid 2px silver"}

![Triangles](images/triangles.png){.center width=50% style="border:solid 2px silver"}

![Disques](images/disques.png){.center width=50% style="border:solid 2px silver"}

Nous utilisons toujours Python et la bibliothèque `p5` dont on rappelle (encore une fois !) les principales instructions[^1] :

[^1]: inspiré de [ce site](https://www.carnets.info/nsi_premiere/parcours_p5/).
    
| Syntaxe                                 | Description                                                                                  |
| :-------------------------------------- | :------------------------------------------------------------------------------------------- |
| `#!py point(x, y)`                      | Point de coordonnées (x, y)                                                                  |
| `#!py line(x1, y1, x2, y2)`             | Segment reliant les points de coordonnées (x1, y1) et (x2, y2)                               |
| `#!py circle(x, y, diamètre)`           | Cercle dont le centre est en (x, y) et le diamètre est fourni                                |
| `#!py square(x, y, côté)`               | Carré dont le sommet en haut à gauche est en (x, y) et ayant le côté fourni                  |
| `#!py rect(x, y, largeur, hauteur)`     | Rectangle dont le sommet en haut à gauche est en (x, y) et dont les dimensions sont fournies |
| `#!py triangle(x1, y1, x2, y2, x3, y3)` | Triangle dont les trois sommets sont en (x1, y1), (x2, y2), et (x3, y3)                      |


On propose ci-dessous une version de base du fichier de jeu. Il va falloir le compléter au fil du TP.

??? question-nico "Le bon fichier"

    1. Télécharger le fichier [tp_5.py](python/tp_5.py){:download="tp_5.py"} ;
   
    2. L'ouvrir avec Thonny ;
   
    3. L'exécuter en cliquant sur ▶️. Installez `p5` si besoin.

!!! warning "Sauvegarde"

    Vous remarquerez une ligne particulière à la fin de la fonction `draw` :
    
    ```python hl_lines="7"
    def draw():
        # Le fond
        background(*BLANC)

        # Votre code

        save_frame("mon_fond_d_ecran.png")  # ne pas oubliez de modifier le nom du fichier !
    ```
    
    C'est cette ligne qui indique à Python de sauvegarder votre image sur le disque dur. On précise en paramètre le nom et l'extension du fichier : `"mon_fond_d_ecran.png"`.
    
    **N'oubliez-pas de modifier ce nom à chaque fois que vous êtes satisfaits d'une figure**.
    
    Si vous ne le faîtes pas, Python va écraser l'ancienne figure sans vous demander votre avis !

L'objectif est vraiment de laisser libre cours à votre imagination. On propose toutefois quelques aides :

??? tip "Des couleurs ?"

    On rappelle qu'une couleur peut être caractérisée par ses trois composantes *Rouge*, *Vert* et *Bleu*. Chacune de ces valeurs est un entier compris entre 0 (inclus) et 256 (exclu).
    
    Par exemple, `#!py (239, 165,26)` est du <span style="color:#FF9400">orange</span>.
    
??? tip "Un entier au hasard"

    Il est possible de générer un nombre au hasard en utilisant la fonction `#!py randrange(mini, maxi)`.
    
    Cette appel renvoie un nombre entier aléatoire compris entre `mini` (inclus) et `maxi` (**exclu**).
    
    Ne pas oublier d'affecter cette valeur à une variable :
    
    ```python
    randrange(10, 20)  # un entier entre 10 et 19 mais perdu car non stocké :(
    
    x = randrange(0, LARGEUR)  # un entier entre 0 et LARGEUR - 1 stocké dans x
    ```
    
??? tip "Des répétitions"

    Les figures présentées plus haut contiennent beaucoup de disques, de triangles, de rectangles (pour le dégradé !).
    
    Heureusement, on n'a pas à déclarer toutes les figures à la main : Python peut effectuer les répétitions à l'aide de la boucle `for`.
    
    La syntaxe est la suivante `#!py for variable in range(debut, fin, pas):` :
    
    * `debut` est la première valeur que prendra la variable ;
    * `fin` est la valeur maximale de la variable, cette valeur sera toujours exclue : la variable augmente jusqu'à l'atteindre ;
    * `pas` est l'écart entre deux valeurs consécutives.
    
    ```python
    for truc in range(0, 10, 3):
        print(truc)  # Affiche 0 -> 3 -> 6 -> 9
    
    for bidule in range(-10, 3, 7):
        print(bidule)  # Affiche -10 -> -3
    ```

Si vous ne savez pas dans quelle direction partir ou si vous êtes à cours d'idées, on propose ci-dessous des aides pour réaliser les images proposées plus haut.

??? info "Le dégradé"

    Il s'agit d'un dégradé allant du bleu `#!py (0, 0, 255)` au noir `#!py (0, 0, 0)`.
    
    Il est en fait composé de nombreux rectangles, très minces, dont les couleurs prennent les différentes teintes entre le bleu et le noir. Tous les rectangles ont pour hauteur la `HAUTEUR` de l'image.
    
    Pour représenter cette figure on doit donc :
    
    * créer une variable indiquant le nombre de rectangles souhaités ;
    * déterminer la largeur de ceux-ci (si on représente `#!py 50` rectangles sur une image de `#!py 800` pixels de large, il en faut `#!py 16`) ;
    * créer une boucle `#!py for` qui effectue parcourt les abscisses entre `#!py 0` et la `#!py LARGEUR` de l'image. On utilise la largeur des rectangles comme pas ;
    * Dans la boucle :
        * calculer la couleur actuelle (prenez un papier et un crayon et faites un tableau de proportionnalité pour trouver la formule) ;
        * dessiner le rectangle !

    Une fois la figure réalisée, vous pouvez aussi modifier sur le couleurs et les dimensions.

    ![Dégradé du jaune au rouge](images/degrades_2.png){.center width=25% style="border:solid 2px silver"}
    

??? info "Les triangles"

    Tous les triangles sont équilatéraux : on débute par choisir la longueur du côté de ceux-ci. Le théorème de Pythagore permet de montrer que si cette longueur vaut $\text{L}$, la hauteur du triangle mesure $\dfrac{\sqrt3}{2}\times \text{L}\approx 0,867 \times \text{L}$.
    
    Il y a beaucoup de triangles ! Si l'on y regarde bien, ceux-ci sont répartis sur des lignes : il y a **beaucoup** de triangles sur **beaucoup** de lignes.
    
    Cette observation nous amène à construire un code autour de **deux** boucles `#!py for` : la première parcourt les lignes de haut en bas, la seconde les colonnes de gauche à droite.
    
    Enfin, connaissant les coordonnées du sommet inférieur gauche d'un triangle ayant la point en haut, il est possible de déterminer les coordonnées des autres sommets :
    
    ![Coordonnées](images/dimension_triangle.png){.center .autolight width=40%}
    
    On peut donc construire le code de la façon suivante :
    
    ```text title="Langage naturel"
    L est la longueur du côté d'un triangle
    
    Pour y allant de 0 à HAUTEUR avec un pas de entier(0,867L) :
        Pour x allant de 0 à LARGEUR avec un pas de L :
            Choisir une couleur aléatoire
            Dessiner le triangle ayant la pointe en haut dont le sommet inf. gauche est en (x, y)
            Choisir une couleur aléatoire
            Dessiner le triangle ayant la pointe en bas dont le sommet sup. gauche est en (x - L/2, y - 0,867L)
    ```
    
??? info "Les disques"

    C'est la figure la plus compliquée : elle contient des disques de rayon aléatoire, tous contenus dans un disque de centre le centre de l'image.
    
    On débute par choisir le rayon `R_GRAND` du disque central (qui contient les petits disques) et le rayon maximal des petits `R_PETITS`.
    
    On choisit alors aléatoirement la position du centre d'un petit disque :
    
    * l'abscisse est un entier aléatoire entre `#!py -R_PETITS` et `R_PETITS`,
    * l'ordonnée est un entier aléatoire entre `#!py -int((R_GRAND**2-R_PETITS**2)**0.5)` et `#!py int((R_GRAND**2-R_PETITS**2)**0.5)`[^2].
    
    [^2]: Cette formule a l'air compliqué mais c'est simplement le résultat du théorème de Pythagore. On précise que $x^{0,5}=\sqrt{x}$.
    
    Le rayon de ce disque est un entier aléatoire entre `#!py 0` et `#!py R_PETITS`.
    
    Ces choix faits, on peut dessiner le disque en prenant soin de décaler son centre vers le centre de la figure: on le « translate » d'un vecteur `#!py (LARGEUR // 2 , HAUTEUR // 2)`.
    
    Il faut répéter ces actions autant de fois que nécessaire avec une boucle `#!py for`.