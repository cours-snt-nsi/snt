---
title: 🏓 Ping-pong
author: N. Revéret
---

# 🏓 Ping-pong

Nous avons découvert dans les premiers TP les bases de `Python`, de la bibliothèque `p5` et la gestion des évènements.

Utilisons-les afin de jouer à Pong !

![Pong !](images/pong.gif){.center width=15%}

 On rappelle les instructions suivantes[^1] de `p5` ainsi que la documentation de [p5 (Shape)](https://p5js.org/reference/#group-Shape).

[^1]: inspiré de [ce site](https://www.carnets.info/nsi_premiere/parcours_p5/).
    
| Syntaxe                                 | Description                                                                                  |
| :-------------------------------------- | :------------------------------------------------------------------------------------------- |
| `#!py point(x, y)`                      | Point de coordonnées (x, y)                                                                  |
| `#!py line(x1, y1, x2, y2)`             | Segment reliant les points de coordonnées (x1, y1) et (x2, y2)                               |
| `#!py circle(x, y, diamètre)`           | Cercle dont le centre est en (x, y) et le diamètre est fourni                                |
| `#!py square(x, y, côté)`               | Carré dont le sommet en haut à gauche est en (x, y) et ayant le côté fourni                  |
| `#!py rect(x, y, largeur, hauteur)`     | Rectangle dont le sommet en haut à gauche est en (x, y) et dont les dimensions sont fournies |
| `#!py triangle(x1, y1, x2, y2, x3, y3)` | Triangle dont les trois sommets sont en (x1, y1), (x2, y2), et (x3, y3)                      |

On propose ci-dessous une version de base du fichier de jeu. Il va falloir le compléter au fil du TP.

??? question-nico "Le bon fichier"

    1. Télécharger le fichier [tp_4.py](python/tp_4.py){:download="tp_4.py"} ;
   
    2. L'ouvrir avec Thonny ;
   
    3. L'exécuter en cliquant sur ▶️. Installez `p5` si besoin.

Les variables utilisées dans ce fichier sont illustrées ci-dessous :

![Dimensions](images/dimensions.png){.center .autolight width=50%}

## Placer la balle

???+ warning "Attention `...`"

    Certaines parties du code restent à compléter. C'est le cas lorsque vous trouvez des points de suspension.
    
??? question-nico "Center la balle"
    
    Donnez des valeurs à `x_balle` et `y_balle` de façon à ce que la balle soit initialement au centre de la fenêtre.
    
    Dans l'idéal, votre code devrait s'adapter à toutes les dimensions de fenêtres... ne tapez pas les dimensions *en dur* mais utilisez plutôt les dimensions de la fenêtre (`LARGEUR` et `HAUTEUR`).

    On rappelle que le double `//` permet de calculer des quotients entiers :

    ```pycon
    >>> 507 // 2
    253
    ```

??? question-nico "Dessiner la balle"

    Ajoutez, dans la fonction `draw`, la ligne permettant de dessiner la balle sous la forme d'un disque centré en `(x_balle, y_balle)` et de diamètre égal à `DIAMETRE`.

## Déplacer la balle

Nous avons une balle à l'écran mais elle est immobile !

Faisons en sorte qu'elle se déplace en complétant la fonction `deplace_balle`.
    
    
??? question-nico "`deplace_balle`"

    L'idée du déplacement est la suivante :
    
    * on ajoute `vx` à `x_balle` ;
    * on ajoute `vy` à `y_balle` ;
    * on vérifie que la balle est bien dans la fenêtre (reportez-vous à l'illustration de la fenêtre au début du TP pour trouvez votre position).
    * si la balle est sortie de la fenêtre, il faut modifier sa vitesse :
        * si elle est trop à gauche ou trop à droite, il faut « renverser » sa vitesse horizontale ,
        * si elle est trop haut ou trop bas, il faut « renverser » sa vitesse verticale.


    Complétez la fonction `deplace_balle`.

    Cette fonction pourra débuter par :

    ```python
    def deplace_balle():
        global x_balle
        global y_balle
        global vx
        global vy
    
        x_balle = x_balle + vx
        y_balle = y_balle + vy
        if x_balle > LARGEUR:
            vx = -vx
        if ...:
            ...
    ```


??? question-nico "Vitesse initiale"

    La balle se déplace initialement trop lentement pour que le jeu soit vraiment *amusant*.
    
    Les vitesses initiales horizontales et verticales de la balle sont stockées dans les variables `vx` et `vy`. On utilise la fonction `random_uniform`
    dont on donne la documentation ci-dessous :
    
    !!! note "p5.random_uniform(high=1, low=0)"

        * Return a uniformly sampled random number.
        * **Parameters**:	

            * high (float) - The upper limit on the random value (defaults to 1).
            * low (float) - The lower limit on the random value (defaults to 0).
        * **Returns**:	
            A random number between low and high.
        
        * Return type:	float

    Faites en sorte que les deux vitesses prennent des valeurs aléatoires dans l'intervalle $\left[3~;~5\right]$.
    

## Placer la raquette

??? question-nico "Dimensionner et placer la raquette"

    Faîtes-en sorte que la raquette :
    
    * mesure un cinquième de la largeur de la fenêtre sur un trentième de sa hauteur ;
    * soit initialement centrée horizontalement ;
    * soit initialement placée à un diamètre de balle du bas de l'écran verticalement.

??? question-nico "Dessiner la raquette"

    Ajoutez, dans la fonction `draw`, la ligne permettant de dessiner la raquette sous la forme d'un rectangle.
    
## Déplacer la raquette

La raquette doit se déplacer en fonction des touches saisies au clavier. Lorsque l'utilisateur :

* appuie sur la flèche de gauche, on soustrait `VITESSE_RAQUETTE` à `x_raquette` ;
* appuie sur la flèche de droite, on ajoute `VITESSE_RAQUETTE` à `x_raquette`.

Ces actions se font dans la fonction `deplace_raquette`.

??? question-nico "`deplace_raquette`"

    Compléter la fonction `deplace_raquette`. On donne la base de code ci-dessous :
    
    ```python
    def deplace_raquette():
        global x_raquette
        if key == "LEFT":
            x_raquette = ...
        ...
    ```

??? question-nico "Rester dans la fenêtre"

    La raquette se déplace mais elle peut sortir de l'écran... 
    
    Il est facile de régler ce problème : il suffit de ne déplacer la raquette que si le déplacement est *valide*.
    
    Un déplacement est valide si, après l'avoir effectué, la raquette ne sort pas de l'écran. On donne la base de code ci-dessous :
    
    ```python
    def deplace_raquette():
        global x_raquette
        if key == "LEFT":
            if ... <= x_raquette - ...:
                x_raquette = ...
        ...
    ```
    
## Les collisions

Nous avons presque fini : il ne reste qu'à gérer les collisions de la balle sur la raquette. Celles-ci sont gérées dans la fonction `collision`.

Quelques questions pour vous guider :

* Supposons que la balle touche le dessus de la raquette. Quelles sont les coordonnées du point de contact en fonction de celles du centre de la balle ?

* Dans quel intervalle doit se trouver l'abscisse du point de contact ?
* Dans quel intervalle doit se trouver l'ordonnée du point de contact ?
* Que se passe-t-il quand la balle rebondit ? Sa position ets modifiée ? Sa vitesse ?

Attention pour finir : la balle peut aussi rebondir sur le dessous de la raquette ou sur les côtés !